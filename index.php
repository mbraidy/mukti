<?php
session_start();
if (isset($_SESSION['USER'])){
    if ($_SESSION['LOGGED']) $_SESSION['SINCE'] = time();
} else {
    $_SESSION = [
        'USER' => "||GUEST||", 'ID' => 0, 'LOGGED' => false, 'SINCE' => 0, 'ATTEMPT' => 0];
}
/**
 * PHP version 7.3
 */

use Core\Router;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require './Core/autoloader.php';

// Invoquing the router
$router = new Router;

$criteria = getenv('QUERY_STRING');
$location = getenv('SCRIPT_URL');

$router->dispatch( $location, $criteria );
