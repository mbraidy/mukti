<?php
    if(!isset($_SESSION)) session_start();

/**
 * @var $viewFile string
 * @var $modalHeader string
 * @var $modalBody string
 */
?>
<!DOCTYPE html>
<html manifest="cache.manifest" lang="sv">
    <head>
        <meta charset="UTF-8">
        <title>Meow</title>
        <link rel="shortcut icon" type="text/css" href="http://localhost/adoption/favicon.ico" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" type="text/css" href="/Assets/fontawesome/css/all.min.css"/>

        <link rel="stylesheet" type="text/css" href="/Assets/css/snackbar.css"/>
        <link rel="stylesheet" type="text/css" href="/Assets/css/index.css">
    </head>
    <body id='container'>
      <header id="header">
          <h1>Meow adoption</h1>
      </header>
      <div class="container" id="main-container">
          <div class="container" id="middle">
          	  <nav class="container" id="left">
          	  	 <?php include("navPanel.php"); ?>
              </nav>
              <article id="mainPage">
                  <?php
                     if (!empty($viewFile))
                        include("App/Views/{$viewFile}.php");
                     else
                        include("App/Views/initial.html");
                  ?>
              </article>
          </div>
          <footer id="footer">
              <p>Meow Adoption</p>
              <p>Contact: 070 - adopt me</p>
          </footer>
      </div>

        <!-- The Modal -->
        <div class="modal fade" id="genericModal">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title"><?= (empty($modalHeader))? '' : $modalHeader; ?></h4>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body" id="modal-body">
                  <?php if (!empty($modalBody)) include( "App/Views/{$modalBody}" ); ?>
              </div>
            </div>
          </div>
        </div>
      <!-- jQuery library -->
      <script type='text/javascript' src="/Assets/js/jquery.min.js"></script>

      <!-- Latest compiled JavaScript -->
      <script type='text/javascript' src="/Assets/js/bootstrap.bundle.min.js"></script>

      <script type='text/javascript' src="/Assets/js/snackbar.js"></script>
      <script type='text/javascript' src="/Assets/js/main.js"></script>
    </body>
</html>
