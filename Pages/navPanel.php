<nav id="main-menu">
    <ul>

<?php if (empty($_SESSION['LOGGED'])) { ?>
        <p class="logged-user">Welcome Guest</p>
        <li><a href="/user/login">Login</a></li>
        <li><a href="/user/signup">Sign up</a></li>

<?php } else { ?>
         <p class="logged-user">Logged in as: <?=$_SESSION['NAME']?>
         <li><a href="/user/profile">Profile</a></li>
         <hr/>

    <?php if ( $_SESSION['ROLE'] == 'admin' ) { ?>
            <li><a href="/cat/color">Cat colors</a></li>
            <li><a href="/cat/pattern">Cat patterns</a></li>
            <li><a href="/cat/race">Cat races</a></li>
            <li><a href="/cat/index">Cats</a></li>
            <hr/>
            <li><a href="/user/users">Users</a></li>
            <hr/>
            <li><a href="/booking/schedule">To do</a></li>
            <li><a href="/booking/history">History</a></li>

    <?php } else { ?>
            <li><a href="/cat/browse">Browse cats</a></li>
            <hr/>
            <li><a href="/booking/schedule">Schedule</a></li>

    <?php } ?>
            <hr/>
            <li><a href="/user/logout">Logout</a></li>

<?php } ?>
        <hr/>
        <li><a href="/home/aboutus">About us</a></li>
        <li><a href="/home/contactus">Contact us</a></li>
    </ul>
</nav>
