<?php

namespace Core;

/**
 * View
 *
 * PHP version 7.3
 */

class View
{
    /**
     * Render a view file
     *
     * @param string $view  The view file
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);

        $file = dirname(__DIR__) . "/$view";

        if (is_readable($file)) {
            require $file;
        } else {
            throw new \Exception("$file not found");
        }
    }

    /**
     * Redirect to a route
     *
     * @param string $url  The route
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function redirect($url, $args = [])
    {
        $argGet = '';
        foreach ($args as $key => $arg) {
            $argGet .= "&{$key}={$arg}";
        }
        if ( !empty($argGet) )
            $argGet = '?'. substr($argGet, 1);

        try {
            header("Location: http://" . getenv('SERVER_NAME') . "{$url}", true, 302);
            exit();
        }
        catch (\exception $e) {
            throw new \Exception("{$e}: {$url} not found");
        }
    }
}
