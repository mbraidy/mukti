<?php

namespace Core;

/**
 * Router
 *
 * PHP version 7.3
 */
class Router
{
    /**
     * Dispatch the route, creating the controller object and running the
     * action method
     *
     * @param string $url The route URL
     *
     * @return void
     */
    static function dispatch( string $address, string $criteria )
    {
        try {
            $params = [];
            $location = 'Home';
            $action = 'welcome';
            if ( !empty( $address ) && strlen($address)>1) {
               $parts = explode( '/', $address, 3 );
               if ( sizeof($parts) != 3 )
                   throw new \Exception("Too few arguments");

               list($dummy, $location, $action) = $parts;
               $location = ucfirst( strtolower( $location ) );
               $action = strtolower( $action );

               if ( !empty($criteria) ) {
                   $separate = explode( '&', $criteria );
                   foreach ( $separate as $item => $value ) {
                       $params[$item] = $value;
                   }
               }
               $pathCheck = "App/Controllers/{$location}Controller.php";
               $path = "\App\Controllers\\{$location}Controller";
               if ( file_exists( $pathCheck ) ) {
                  $controller = new $path;
                  if (    method_exists( $controller, $action )
                       && is_callable( [ $controller, $action ] ) ) {

                      $result = $controller->{$action}( $params );

                      if ($result !== "NOTLOGGEDIN") {
                         return;
                      }
                   }
               }
            }
            throw new \Exception();
        } catch (\Exception $exceotion) {
            $path = "\App\Controllers\UserController";
            $controller = new $path;
            $controller->login();
        }

    }
}

