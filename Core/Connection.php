<?php

namespace Core;

use App\Database\Connect;
use PDO;

/**
 * Model to connect to a databse
 */

class Connection
{
    /**
     * Get the PDO database connection
     *
     * @return PDO
     */
    public static function openLink() : PDO
    {
        try {
            $dsn = "mysql:host=" . Connect::DB_HOST . ";dbname=" . Connect::DB_NAME . ";charset=utf8";
            $db = new PDO($dsn, Connect::DB_USER, Connect::DB_PASSWORD, [
                PDO::ATTR_PERSISTENT => true
                ]
            );

            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch (\PDOException $e){
            echo "Error connecting!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
