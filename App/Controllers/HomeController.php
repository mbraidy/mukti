<?php

namespace App\Controllers;

use Core\View;

/**
 * Sequential controller
 *
 * PHP version 7.3
 */
class HomeController
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function welcome()
    {
        View::render('Pages/start.php');
    }
    public function contactus()
    {
        View::render('/Pages/start.php', [
            'viewFile' =>  'home/contactus',
        ]);
    }
    public function aboutus()
    {
        View::render('/Pages/start.php', [
            'viewFile' =>  'home/aboutus',
        ]);
    }

}
