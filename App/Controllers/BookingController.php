<?php

namespace App\Controllers;

use App\Models\User;
use Core\View;
use App\Models\CatRace;
use App\Models\Cat;
use App\Models\Booking;

/**
 * Sequential controller
 *
 * PHP version 7.3
 */
class BookingController
{
    /**
     * Show the index page
     *
     * @return void
     */

    public function book()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $cat = new Cat();
        $race = new CatRace();

        if ( $catResult = $cat->find($_GET["id"], $_GET["detail"]) ) {
            if ( $catRace = $race->find($catResult["raceID"]) ) {
                echo json_encode([
                    "result" => "FOUND",
                    "cat" => $catResult,
                    "race" => $catRace,
                ]);
            } else {
                echo json_encode([
                    "result" => "NOT FOUND",
                    "message" => "Race has not been found.  Please contact the company about that!"
                ]);
            }
        } else {
            echo json_encode([
                "result" => "NOT FOUND",
                "message" => "Cat has not been found and cannot be set for adoption!"
            ]);
        }
    }
    public function schedule()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $booking = new Booking();

        View::render('/Pages/start.php', [
            'viewFile' =>  (User::isAdmin())?'Calendar/monitor':'Calendar/schedule',
            'schedule' => $booking->getFullSchedule(),
            'logged' => true,
        ]);
    }
    public function history()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $booking = new Booking();

        View::render('/Pages/start.php', [
            'viewFile' => 'Calendar/history',
            'schedule' => $booking->getHistory(),
            'logged' => true,
        ]);
    }
    public function getSchedule()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        if ( empty($_GET) || empty($_GET["date"])) {
            echo json_encode([
                "result" => "EMPTY",
                "message" => "No time was selected"
            ]);
        } else {
            $day = new Booking();
            echo json_encode([
                "result" => "FOUND",
                "day" => $day->find($_GET["date"])
            ]);
        }
    }
    public function save()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $booking = new Booking();

            if ( $booking->create() )
                echo json_encode([
                    "result" => "CREATED",
                    "message" => "Booking updated successfully!"
                ]);
            else
                echo json_encode([
                    "result" => "FAILURE",
                    "message" => "Booking was not updated"
                ]);
        }
    }
    public function delete()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER["REQUEST_METHOD"] == "DELETE") {
            $booking = new Booking();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $booking->delete($data["date"], $data["timing"], $data['cat']) )
                echo json_encode([
                    "result" => "DELETED",
                    "message" => "Booking deleted successfully!"
                ]);
            else
                echo json_encode([
                    "result" => "FAILURE",
                    "message" => "Booking was not deleted"
                ]);
        }
    }
    public function adopt()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER["REQUEST_METHOD"] == "PUT") {
            $booking = new Booking();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $booking->adopt($data["date"], $data["timing"], $data['cat'], $data['adopter']) )
                echo json_encode([
                    "result" => "ADOPTED",
                    "message" => "Cat adopted successfully!"
                ]);
            else
                echo json_encode([
                    "result" => "FAILURE",
                    "message" => "Adoption was not deleted"
                ]);
        }
    }
    public function noshow()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER["REQUEST_METHOD"] == "PUT") {
            $booking = new Booking();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $booking->noshow($data["date"], $data["timing"], $data['cat'], $data['adopter']) )
                echo json_encode([
                    "result" => "CLEARED",
                    "message" => "Schedule was cleared successfully!"
                ]);
            else
                echo json_encode([
                    "result" => "FAILURE",
                    "message" => "Schedule was not cleared"
                ]);
        }
    }
    public function nointerest()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER["REQUEST_METHOD"] == "PUT") {
            $booking = new Booking();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $booking->nointerest($data["date"], $data["timing"], $data['cat'], $data['adopter']) )
                echo json_encode([
                    "result" => "CLEARED",
                    "message" => "Schedule was cleared successfully!"
                ]);
            else
                echo json_encode([
                    "result" => "FAILURE",
                    "message" => "Schedule was not cleared"
                ]);
        }
    }
}
