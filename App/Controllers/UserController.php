<?php

namespace App\Controllers;

use App\Models\User;
use \Core\View;

/**
 * User controller
 *
 * PHP version 7.3
 */
class UserController
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function login()
    {
        if ( empty($_POST) ) {
            View::render('/Pages/start.php', [
                'viewFile' => 'User/login',
                'logged' => false
            ]);
        }

        switch ( User::login() ) {
            case 'SUCCESS':
                if (User::isAdmin())
                    View::redirect("/Cat/index", [
                        'logged' => true
                    ]);
                else
                    View::redirect("/Cat/browse", [
                        'logged' => true
                    ]);
                break;
            case 'SIGNUP':
                View::render('/Pages/start.php', [
                    'viewFile' =>  'User/signup',
                    'logged' => false,
                ]);
                break;
            case 'WAIT':
                View::render('/Pages/start.php', [
                    'viewFile' =>  'User/login',
                    'logged' => false,
                    'waitTime' => $_SESSION['SINCE'] + 1800 - time()
                ]);
                break;
            case 'WRONG':
                View::render('/Pages/start.php', [
                    'viewFile' =>  'User/login',
                    'logged' => false,
                ]);
        }
    }
    public function signup()
    {
        if (empty($_POST)) {
            View::render('/Pages/start.php', [
                'viewFile' =>  'User/signup',
                'logged' => false,
                'modalHeader' => 'Terms and conditions',
                'modalBody' => 'Document/member.php',

            ]);
            return true;
        }
        switch ($message=User::signup()) {
            case 'SUCCESS':
                echo json_encode(['result' => 'REGISTERED']);
                break;
            case 'FOUND':
                echo json_encode(['result' => 'DUPLICATE']);
                break;
            default:
                echo json_encode(['result' => 'ERROR', 'message' => $message]);
        }
    }
    public function logout()
    {
        switch ($message=User::logout()) {
            case 'LOGOUT':
                View::redirect('/Pages/start.php');
                break;
            default:
                return $message;
        }
    }
    public function update()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $users = new User();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $users->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'User updated successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'User was not updated'
                    ]);
        }
    }

    public function profile()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
         $profile = new User();
         $row = $profile->find();
         if ( empty($_POST) ) {
             View::render('/Pages/start.php', [
                 'viewFile' =>  'User/profile',
                 'data' => $row,
                 'logged' => true,
                 'modalHeader' => 'Change password',
                 'modalBody' => 'User/changePassword.php',
             ]);
         } else {
             $row[$_POST['field']] = $_POST['value'];
             $profile->update( $_POST['field'], $_POST['value'] );

             echo json_encode(['result' => 'UPDATED']);

         }
     }
     public function users()
     {
         if (!User::isLogged()) return "NOTLOGGEDIN";
         if (!User::isAdmin()) return false;
         $users = new User();
         $rows = $users->findAll();

         View::render('/Pages/start.php', [
             'viewFile' =>  'User/users',
             'data' => $rows,
             'logged' => true,
             'modalHeader' => 'Add User',
             'modalBody' => 'User/edituser.php',
         ]);
     }
     public function delete()
     {
         if (!User::isLogged()) return "NOTLOGGEDIN";

         if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
             $user = new User();
             $data = json_decode(file_get_contents("php://input"), true);
             if ( $user->delete($data['id']) )
                 echo json_encode([
                     'result' => 'DELETED',
                     'message' => 'User deleted successfully!'
                 ]);
                 else
                     echo json_encode([
                         'result' => 'FAILURE',
                         'message' => 'User was not deleted'
                     ]);
         }
     }

     public function change_password()
     {
         if (!User::isLogged()) return "NOTLOGGEDIN";
         if ( $_SESSION['ATTEMPT'] >= 3 ) {
             if ( time() - $_SESSION['SINCE'] < 1800 ) {
                 echo json_encode(['result' => 'WAIT']);
                 return;
             } else {
                 $_SESSION['ATTEMPT'] = 0;
             }
         }
         $profile = new User();
         $row = $profile->find();
         if ( empty($_POST) ) {
             View::render('/Pages/start.php', [
                 'viewFile' =>  'User/profile',
                 'data' => $row,
                 'logged' => true,
                 'modalHeader' => 'Change password',
                 'modalBody' => 'User/changePassword.php',
             ]);
         } else {
             if ( $row['password'] != $_POST['oldpass'] ) {
                 ++$_SESSION['ATTEMPT'];
                 $_SESSION['SINCE'] = time();
                 echo json_encode(['result' => 'WRONG']);
                 return;
             }
             if ( $_POST['password'] != $_POST['pswRepeat'] ) {
                 echo json_encode(['result' => 'REPEAT']);
                 return;
             }
             $row['password'] = $_POST['password'];
             $profile->update( 'password', $_POST['password'] );

             echo json_encode(['result' => 'UPDATED']);
         }
     }
}

