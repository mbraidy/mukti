<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\CatColor;
use Core\View;
use App\Models\CatPattern;
use App\Models\CatRace;
use App\Models\Cat;

/**
 * Sequential controller
 *
 * PHP version 7.3
 */
class CatController
{
    /**
     * Show the index page
     *
     * @return void
     */

    public function index()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        $cat = new Cat();
        $raceModel = new CatRace();
        $colorModel = new CatColor();
        $races = $raceModel->getRaces();
        $colors = $colorModel->getColors();
        if ( empty($_POST) ) {
            $row = $cat->getcats();
            View::render('/Pages/start.php', [
                'viewFile' =>  'Cat/cat',
                'races' => $races,
                'colors' => $colors,
                'data' => $row,
                'logged' => true,
                'admin' => User::isAdmin(),
                'modalHeader' => 'Add a cat',
                'modalBody' => 'Cat/add_cat.php',
            ]);
        } else {
            if ( $id = $cat->create() ) {
                $raceModel = new CatRace();
                $colorModel = new CatColor();
                $race = $raceModel->getRace($_POST['raceID']);
                $color = $colorModel->getColor($_POST['colorID']);
                $picture = "";
                if ( !empty($_POST['picture']) ) {
                    $picture = "/Assets/images/cat/{$_POST['picture']}";
                }
                $is_available = ($_POST['available'])? 'fa-check-circle text-success' : 'fa-times-circle text-danger';
                $is_booked = ($_POST['booked'])? 'fa-calendar-check text-success' : 'fa-calendar-times text-danger';
                $data = "<tbody class='data-table' id='tbody-{$id}'>
                            <tr class='parent'>
                            <td contenteditable='true' onblur='update_row(this.id)'
                                id='parent_name-{$id}'>
                                {$_POST['name']}
                            </td>
                            <td id='parent_raceID-{$id}'>{$race}</td>
                            <td><img src='{$picture}'
                                     class='zoom' alt='No image yet' height='36'
                                     id='parent_picture-{$id}'/>
                            </td>
                            <td class='text-center'>
                               <i class='far {$is_available}'></i>
                            </td>
                            <td class='text-center'>
                               <i class='far {$is_booked}'></i>
                           </td>
                            <td id='parent_gender-{$id}'>{$_POST['gender']}</td>
                            <td nowrap>
                               <button type='button' onclick='delete_row(this.id)'
                            	       data-ref='/cat/delcat'
                                       id='del-{$id}'
                                       class='delete_row btn btn-link'>
                                     <i class='far fa-trash-alt text-danger'></i>
                               </button>
                               <button type='button'
                                       data-ref='/cat/updatecat'
                                       id='update-{$id}'
                                       class='update_row btn btn-link'>
                                     <i class='far fa-eye text-primary'></i>
                               </button>
                            </td>
                        </tr>
                        <tr class='child'>
                            <td colspan=7>
                              <section class='editcat-container'>
                                <label for='name-{$id}' class='cat-head-left'><b>Cat</b></label>
                                <label for='raceID-{$id}' class='cat-head-right'><b>Race</b></label>
                                <input type='text' class='cat-input-left input-field' name='name'
                                       onblur='update_row(this.id, \"parent\")'
                                       placeholder='Enter the cat''s name' id='name-{$id}' required
                                       value='{$_POST['name']}'/>
                                <select class='cat-input-right input-field'
                                        onblur='update_row(this.id, \"parent\")'
                                        name='raceID' id='raceID-{$id}'>
                                   <option value='{$_POST['raceID']}' disabled>Select the race...</option>";
                foreach ( $races as $race ) {
                    $data .= "<option value='{$race['id']}'" .
                                ($_POST['raceID'] == $race['id'] ? 'selected>' : '>') .
                                  $race['name'] .
                             "</option>";
                }
                $data .= "</select>
                            <label for='dob-{$id}' class='cat-head-left'><b>DoB</b><span id='age-{$id}'></span></span></label>
                            <label for='colorID' class='cat-head-right'><b>Color</b></label>
                            <input type='date' class='cat-input-left input-field' name='dob'
                                   placeholder='Date of birth' id='dob-{$id}' required onchange='update_age(this.value)'
                                   onchange='update_row(this.id)'
                                   min='2010-01-01' max='{date('Y-m-d')}' value='{$_POST['dob']}'/>
                            <select class='cat-input-right input-field'
                                    onchange='update_row(this.id)'
                                    name='colorID' id='colorID-{$id}'>
                               <option value='' disabled>Select the color...</option>";
                foreach ( $colors as $color ) {
                    $data .= "<option value='{$color['id']}'" .
                                ($_POST['colorID'] == $color['id'] ? 'selected>' : '>') .
                                $color['name'] .
                             "</option>";
                }
                $data .= "</select>
                          <label for='chip' class='cat-head-left'><b>Chip number</b></label>
                          <label for='gender' class='cat-head-right'><b>Gender</b></label>
                          <input type='text' class='cat-input-left input-field' name='chip'
                                 placeholder='Enter the cat''s chip number' id='chip-{$id}' required
                                 onchange='update_row(this.id)'
                                 value='{$_POST['chip']}'/>
                          <select class='cat-input-right input-field'
                                  onchange='update_row(this.id)'
                                  name='gender' id='gender-{$id}'>
                                <option value='' disabled>Select the gender...</option>";
                if ( $_POST['gender'] == 'male' ) {
                    $data .= "<option value='female'>Female</option>
                              <option value='male' selected>Male</option>";
                } else {
                    $data .= "<option value='female' selected>Female</option>
                              <option value='male'>Male</option>";
                }
                $data .= "</select>
                          <label for='note' class='cat-head-left'><b>Notes</b></label>
                          <textarea class='cat-input-wide input-field' name='note'
                                    placeholder='Enter notes' id='note-{$id}' rows='5' cols='50'>
                          </textarea>
                          <br/>
                          <label class='cat-head-left'><b>Pictures</b></label>
                          <label class='cat-head-right1'><b>Available</b></label>
                          <label class='cat-head-right2'><b>Booked</b></label>
                          <input type='file' id='picture-{$id}' name='picture'
                                 class='cat-input-left input-field'
                                 onchange='handleFileSelect(this, \"parent\")'/>
                          <output for='show-picture-{$id}' class='cat-input-right'
                                  id='output-picture-{$id}'>{$_POST['picture']}</output>

                          <img src='/Assets/images/cat/{$_POST['picture']}' id='show-picture-{$id}'
                               class='cat-input-left' alt='No image yet' height='100'/>

                          <div class='material-switch cat-input-right1'>
                              <input type='checkbox' class='checkbox-hide input-field {$is_available}'
                                     onchange='update_row(this.id, 'parent')'
                                     name='available' id='available-{$id}'>
                              <label for='available-{$id}' class='label-success'></label>
                          </div>
                          <div class='material-switch cat-input-right2'>
                              <input type='checkbox' class='checkbox-hide input-field {$is_booked}'
                                     onchange='update_row(this.id, \"parent\")'
                                     name='booked' id='booked-{$id}'>
                              <label for='booked-{$id}' class='label-success'></label>
                          </div>
                          <br/>
                        </section>
                      </td>
                  </tr>
                  </tbody>";
                echo json_encode([
                    'result' => 'CREATED',
                    'message' => 'Cat created successfully!',
                    'data' => $data]);
            } else {
                echo json_encode([
                    'result' => 'FAILURE',
                    'message' => 'Cat was not created'
                ]);
            }
        }
    }
    public function browse()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $cat = new Cat();

        $races = new CatRace();
        $colors = new CatColor();
        $row = $cat->getcats();
        View::render('/Pages/start.php', [
            'viewFile' =>  'Cat/browse',
            'races' => $races->getRaces(),
            'colors' => $colors->getColors(),
            'data' => $row,
            'logged' => true,
            'admin' => User::isAdmin(),
            'modalHeader' => 'Book a meeting',
            'modalBody' => 'Calendar/book.php',
        ]);
    }
    public function viewcat()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $cat = new Cat();

            if ( empty( $_GET['detail'] ) )
                $detail = false;
            else
                $detail = $_GET['detail'];

            if ( $result = $cat->find( $_GET['id'], $detail ) ) {
                echo json_encode([
                    'result' => 'FOUND',
                    'data' => $result
                ]);
            } else {
                echo json_encode([
                    'result' => 'NOT FOUND',
                    'message' => 'Cat not found!'
                ]);
            }
        }
    }
    public function updatecat()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $cats = new Cat();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $cats->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'Cat updated successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Cat was not updated'
                    ]);
        }
    }
    public function updatewholecat()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $cats = new Cat();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $cats->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'Cat updated successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Cat was not updated'
                    ]);
        }
    }
    public function delcat()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $cat = new Cat();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $cat->delete($data['id']) )
                echo json_encode([
                    'result' => 'DELETED',
                    'message' => 'Cat deleted successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Cat was not deleted'
                    ]);
        }
    }
    public function color()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $colors = new CatColor();

        if ( empty($_POST) ) {
            $row = $colors->getColors();
            View::render('/Pages/start.php', [
                'viewFile' =>  'Cat/color',
                'data' => $row,
                'logged' => true,
                'modalHeader' => 'Add a cat color',
                'modalBody' => 'Cat/editcolor.php',

            ]);
        } else {
            if ( $id = $colors->create() )
                echo json_encode([
                    'result' => 'CREATED',
                    'message' => 'Color created successfully!',
                    'data' => "<tr>".
                                "<td contenteditable='true' onblur='update_row(this.id)'" .
                                          "id='name-{$id}'>" .
                                      $_POST['name'] .
                                "</td>" .
                                "<td contenteditable='true' onblur='update_row(this.id)'" .
                                          "id='note-{$id}'>" .
                                      $_POST['note'] .
                                "</td>" .
                                "<td>" .
                                    "<button type='button' onclick='delete_row(this.id)'" .
                                    	       " data-ref='/cat/delcolor'" .
                                    	       " id='del-{$id}'" .
                                               " class='delete_row btn btn-link'>" .
                                               "<i class='far fa-trash-alt text-danger'></i>" .
                                    "</button>" .
                                "</td>" .
                            "</tr>"]);
            else
                echo json_encode([
                    'result' => 'FAILURE',
                    'message' => 'Color was not created'
                ]);
        }
    }
    public function updatecolor()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $colors = new CatColor();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $colors->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'Color updated successfully!'
                ]);
            else
                echo json_encode([
                    'result' => 'FAILURE',
                    'message' => 'Color was not updated'
                ]);
        }
    }
    public function delcolor()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $color = new CatColor();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $color->delete($data['id']) )
                echo json_encode([
                    'result' => 'DELETED',
                    'message' => 'Color deleted successfully!'
                ]);
            else
                echo json_encode([
                    'result' => 'FAILURE',
                    'message' => 'Color was not deleted'
                ]);
        }
    }
    public function pattern()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $pattern = new CatPattern();

        if ( empty($_POST) ) {
            $row = $pattern->getpatterns();
            View::render('/Pages/start.php', [
                'viewFile' =>  'Cat/pattern',
                'data' => $row,
                'logged' => true,
                'modalHeader' => 'Add a cat pattern',
                'modalBody' => 'Cat/editpattern.php',

            ]);
        } else {
            if ( $id = $pattern->create() )
                echo json_encode([
                    'result' => 'CREATED',
                    'message' => 'Pattern created successfully!',
                    'data' => "<tr>".
                            "<td contenteditable='true' onblur='update_row(this.id)'" .
                                "id='name-{$id}'>" .
                                $_POST['name'] .
                            "</td>" .
                            "<td contenteditable='true' onblur='update_row(this.id)'" .
                                "id='note-{$id}'>" .
                                $_POST['note'] .
                            "</td>" .
                            "<td>" .
                                "<button type='button' onclick='delete_row(this.id)'" .
                                    " data-ref='/cat/delpattern'" .
                                    " id='del-{$id}'" .
                                    " class='delete_row btn btn-link'>" .
                                    "<i class='far fa-trash-alt text-danger'></i>" .
                                "</button>" .
                            "</td>" .
                        "</tr>"]);
                    else
                        echo json_encode([
                                'result' => 'FAILURE',
                                'message' => 'Pattern was not created'
                            ]);
        }
    }
    public function updatepattern()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $patterns = new CatPattern();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $patterns->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'Pattern updated successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Pattern was not updated'
                    ]);
        }
    }
    public function delpattern()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $pattern = new CatPattern();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $pattern->delete($data['id']) )
                echo json_encode([
                    'result' => 'DELETED',
                    'message' => 'Pattern deleted successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Pattern was not deleted'
                    ]);
        }
    }
    public function race()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";
        $race = new CatRace();

        if ( empty($_POST) ) {
            $row = $race->getraces();
            View::render('/Pages/start.php', [
                'viewFile' =>  'Cat/race',
                'data' => $row,
                'logged' => true,
                'modalHeader' => 'Add a cat race',
                'modalBody' => 'Cat/add_race.php',
            ]);
        } else {
            if ( $id = $race->create() )
                echo json_encode([
                    'result' => 'CREATED',
                    'message' => 'Race created successfully!',
                    'data' => "<tr>
                                 <td contenteditable='true' onblur='update_row(this.id)'
                                     id='name-{$id}'>
                                     {$_POST['name']}
                                 </td>
                                 <td contenteditable='true' onblur='update_row(this.id)'
                                  id='usefulness-{$id}'>
                                    {$_POST['playfulness']}
                                 </td>
                                 <td>
                                    <img src='/Assets/images/race/{$_POST['picture1']}'
                                        class='zoom' alt='No image yet' height='36'/>
                                 </td>
                                 <td>
                                    <img src='/Assets/images/race/{$_POST['picture2']}'
                                        class='zoom' alt='No image yet' height='36' />
                                 </td>
                                 <td>
                                    <button type='button' onclick='delete_row(this.id)'
                                            data-ref='/cat/delrace'
                                            id='del-{$id}'
                                            class='delete_row btn btn-link'>
                                       <i class='far fa-trash-alt text-danger'></i>
                                    </button>
                                    <button type='button' onclick='view_row(this.id)'
                                            data-ref='/cat/updaterace'
                                            id='update-{$id}'
                                            class='update_row btn btn-link'>
                                     <i class='far fa-eye text-primary'></i>
                                    </button>
                                </td>
                            </tr>"]);
                    else
                        echo json_encode([
                            'result' => 'FAILURE',
                            'message' => 'Race was not created'
                        ]);
        }
    }
    public function viewrace()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $race = new CatRace();

            if ( $result = $race->find($_GET['id']) ) {
                echo json_encode([
                    'result' => 'FOUND',
                    'data' => $result
                ]);
            } else {
                echo json_encode([
                    'result' => 'NOT FOUND',
                    'message' => 'Race not found!'
                ]);
            }
        }
    }
    public function updaterace()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $races = new CatRace();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $races->update( $data['id'], $data['field'], $data['value'] ) )
                echo json_encode([
                    'result' => 'UPDATED',
                    'message' => 'Race updated successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Race was not updated'
                    ]);
        }
    }
    public function delrace()
    {
        if (!User::isLogged()) return "NOTLOGGEDIN";

        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $race = new CatRace();
            $data = json_decode(file_get_contents("php://input"), true);
            if ( $race->delete($data['id']) )
                echo json_encode([
                    'result' => 'DELETED',
                    'message' => 'Race deleted successfully!'
                ]);
                else
                    echo json_encode([
                        'result' => 'FAILURE',
                        'message' => 'Race was not deleted'
                    ]);
        }
    }
}
