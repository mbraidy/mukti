<?php

namespace App\Models;

use Core\Connection;
use PDO;

/**
 * User model
 *
 * PHP version 7.3
 */
class User
{
    private $fields = [
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'phone' => 'phone',
            'terms' => 'terms',
            'username' => 'username'
            ];
    private static function tableName() : string
    {
        return "user";
    }

    public static function login()
    {
        if (empty($_POST) || empty($_POST['username']) || empty($_POST['password']))
            return false;

        if ( $_SESSION['ATTEMPT'] >= 3 ) {
            if ( time() - $_SESSION['SINCE'] < 1800 ) {
               return "WAIT";
            } else {
               $_SESSION['ATTEMPT'] = 0;
            }
        }
        $query = "SELECT *
                    FROM `" . self::tableName() . "`
                   WHERE `username` = :username";
        try {
            $db = Connection::openLink();

            $cursor = $db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':username' => $_POST['username']]);

            $row = $cursor->fetch();
            if (!$row) {
                ++$_SESSION['ATTEMPT'];
                $_SESSION['SINCE'] = time();
                return "SIGNUP";
            }

            if ($row['password'] != $_POST['password']) {
                ++$_SESSION['ATTEMPT'];
                $_SESSION['SINCE'] = time();
                return "WRONG";
            }

            $_SESSION['USER'] = $_POST['username'];
            $_SESSION['NAME'] = $row['name'];
            $_SESSION['ID'] = $row['id'];
            $_SESSION['LOGGED'] = true;
            $_SESSION['SINCE'] = time();
            $_SESSION['ATTEMPT'] = 0;
            $_SESSION['ROLE'] = $row['role'];

            return 'SUCCESS';
        } catch ( \PDOException $e ) {
            echo "Error connecting!: ", $e->getMessage(), "<br/>";
            die();
        }
    }

    public static function signup()
    {
        if (empty($_POST) || empty($_POST['username']) || empty($_POST['password']))
            return false;

            $db = Connection::openLink();
            $username = $_POST['username'];
            $found = $db->prepare("SELECT id
                                     FROM `" . self::tableName() . "`
                                    WHERE username = :username");
            $found->execute(['username' => $username]);
            if ($found->fetchColumn()) {
                return "FOUND";
            }

            $user = $db->prepare("INSERT INTO
                                   `" . self::tableName() . "` (username, password, role)
                                   VALUES (:username, :password, :role)");
            $user->execute([
                ':username' => $username,
                ':password' => $_POST['password'],
                ':role' => $_POST['role']
            ]);

            $_SESSION['USER'] =null;
            $_SESSION['NAME'] = null;
            $_SESSION['ID'] = null;
            $_SESSION['LOGGED'] = false;
            $_SESSION['SINCE'] = 0;
            $_SESSION['ATTEMPT'] = 0;
            $_SESSION['ROLE'] = '';
            return 'SUCCESS';
    }
    public static function logout()
    {
            $_SESSION['USER'] = '__GUEST__';
            $_SESSION['NAME'] = "Guest";
            $_SESSION['ID'] = null;
            $_SESSION['LOGGED'] = false;
            $_SESSION['SINCE'] = 0;
            $_SESSION['ATTEMPT'] = 0;
            $_SESSION['ROLE'] = '';
            return 'LOGOUT';
    }

    public static function create()
    {
        if (empty($_GET) || empty($_GET['username']))
            return "empty";

            $db = Connection::openLink();
            $username = $_GET['username'];
            $found = $db->prepare("SELECT id
                                     FROM `" . self::tableName() . "`
                                    WHERE username = :username");
            $found->execute(['username' => $username]);
            return $found->fetchColumn();
    }
    public function find() : array
    {
        if ( empty($_SESSION) || empty($_SESSION['LOGGED']) )
            return [];

        $db = Connection::openLink();
        $cursor = $db->prepare("SELECT *
                                 FROM `" . self::tableName() . "`
                                WHERE id = :id");
        $cursor->execute([':id' => $_SESSION['ID']]);
        if ($row = $cursor->fetch( PDO::FETCH_ASSOC ))
            return $row;
        return [];
    }
    public function findAll() : array
    {
            $db = Connection::openLink();
            $cursor = $db->prepare("SELECT *
                                 FROM `" . self::tableName() . "`");
            $cursor->execute();
            if ($row = $cursor->fetchAll( PDO::FETCH_ASSOC ))
                return $row;
            return [];
    }

    public function update( $userID, $field, $value )
    {
        try {
            $db = Connection::openLink();
            $cursor = $db->prepare("UPDATE `" . self::tableName() . "`
                                       SET `{$this->fields[$field]}` = :value
                                    WHERE id = :id");
            $cursor->execute([
                        ':id' => $userID,
                        ':value' =>  $value,
                    ]);
            return true;
        } catch ( \PDOException $e ) {
            echo "Error Updating!: ", $e->getMessage(), "<br/>";
            die();
        }

    }
    /**
     * @return bool
     */
    public function delete( $id ) : bool
    {
        $query = "DELETE FROM `{$this->tableName()}` WHERE `id` = :id";

        try {
            $db = Connection::openLink();
            $cursor = $db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $id]);
            return true;
        } catch (\PDOException $e) {
            echo "Error deleting a user: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    public static function isLogged()
    {
        return ( !empty($_SESSION) && $_SESSION['LOGGED'] );
    }

    public static function isAdmin()
    {
        return ( !empty($_SESSION) && $_SESSION['ROLE']==='admin' );
    }
}
