<?php

namespace App\Models;

use PDO;
use Core\Connection;

/**
 * This is the model class for table "cat".
 *
 * @property int $id
 * @property string $name
 * @property int $raceID
 * @property colorID $colorID
 * @property date $dob
 * @property string $note
 * @property string $chip
 * @property string $picture
 * @property colorID $colorID
 * @property boolean $booked
 * @property boolean $available
 * @property string $gender
 *
 * @property Comment[] $getComments
 * @property Comment[] $getComment
 * @property bool $addComment
 * @property bool $delComment
 * @property string $getName;
 * @property int $getTime;
 * @property int $getID;
 * @property int $getCount;
 */
class Cat
{
    public function __construct( ) {
        $this->db = Connection::openLink();
    }

    private function tableName() : string
    {
        return "cat";
    }

    /**
     * @return \App\Models\Cat
     */
    public function getCats() : array
    {
        $query = "SELECT DISTINCTROW cat.*,
                         cat_race.name AS race,
                         cat_color.name AS color
                    FROM `" . self::tableName() . "`
                    LEFT JOIN cat_race ON raceID = cat_race.id
                    LEFT JOIN cat_color ON colorID = cat_color.id";
        if (!User::isAdmin()) {
            $query .= " LEFT JOIN booking ON booking.catID = cat.id
                        WHERE available = 1
                          AND (   booked = 0
                               OR userID = :userID)";
        }
        $query .= " ORDER BY available DESC, booked DESC";
        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':userID' => $_SESSION['ID']]);
            $cats = $cursor->fetchAll(PDO::FETCH_ASSOC);
            foreach ($cats as $index => $cat) {
                $cats[$index]['dob'] = substr($cat['dob'], 0, 10);
            }
            return $cats;
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return \App\Models\Cat
     */
    public function find(int $id, bool $detail = false) : array
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`
                   WHERE id = :id";

        if ($detail) {
            $query = "SELECT cat.*,
                             cat_race.name AS race,
                             cat_color.name AS color,
                             cat_race.note AS details
                        FROM `" . self::tableName() . "`
                        JOIN cat_race ON raceID = cat_race.id
                        JOIN cat_color ON colorID = cat_color.id
                       WHERE cat.id = :id";
        }
        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute( [':id' => $id] );
            $cat = $cursor->fetch(PDO::FETCH_ASSOC);
            $cat['dob'] = substr($cat['dob'], 0, 10);
            return $cat;
        } catch ( \PDOException $e ) {
            echo "Error retrieving cats!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function create( ) : int
    {
        $query = "INSERT INTO `" . $this->tableName() . "`
                              ( `name`, `raceID`, `colorID`, `dob`, `note`, `chip`, `picture`, `booked`, `available`, `gender` )
                       VALUES ( :name, :raceID, :colorID, :dob, :note, :chip, :picture, :booked, :available, :gender )";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':name' => $_POST['name'],
                ':raceID' => $_POST['raceID'],
                ':colorID' => $_POST['colorID'],
                ':dob' => $_POST['dob'],
                ':note' => $_POST['note'],
                ':chip' => $_POST['chip'],
                ':picture' => $_POST['picture'],
                ':booked' => $_POST['booked'],
                ':available' => $_POST['available'],
                ':gender' => $_POST['gender'],
            ]);
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            echo "Error creating a cat: ", $e->getMessage(), "<br/>";
            die();
        }
    }
    /**
     * @return bool
     */
    public function update( int $id, string $field, string $value ) : bool
    {
        $query = "UPDATE `" . $this->tableName() . "`
                     SET `{$field}` = :value
                   WHERE id = :id";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':value' => $value,
                ':id' => $id
            ]);
            return true;
        } catch (\PDOException $e) {
            echo "Error updating a cat: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete( $id ) : bool
    {
        $query = "DELETE FROM `{$this->tableName()}` WHERE `id` = :id";

        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $id]);
            return true;
        } catch (\PDOException $e) {
            echo "Error deleting a race: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
}
