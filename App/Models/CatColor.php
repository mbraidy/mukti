<?php

namespace App\Models;

use PDO;
use Core\Connection;

/**
 * This is the model class for table "cat-color".
 *
 * @property int $id
 * @property string $name
 * @property string $note
 *
 * @property CatColor[] $getColors
 * @property string $getColor
 * @property bool $create
 * @property bool $delete
 * @property bool $update
 */
class CatColor
{
    private $id;
    private $name;
    private $note;

    private $db;

    public function __construct( ) {
        $this->db = Connection::openLink();
    }

    private function tableName() : string
    {
        return "cat_color";
    }

    /**
     * @return \App\Models\CatColor
     */
    public function getColors() : array
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute();

            return $cursor->fetchAll(PDO::FETCH_ASSOC);
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return string
     */
    public function getColor(int $color_id) : string
    {
        $query = "SELECT name
                    FROM `" . self::tableName() . "`
                   WHERE `id` = :colorID";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':colorID' => $color_id]);

            return $cursor->fetchColumn();
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function create( ) : bool
    {
        $query = "INSERT INTO {$this->tableName()}
                              ( name, note )
                       VALUES ( :name, :note )";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':name' => $_POST['name'],
                ':note' => $_POST['note'],
            ]);
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            echo "Error creating a color: ", $e->getMessage(), "<br/>";
            die();
        }
    }
    /**
     * @return bool
     */
    public function update( int $id, string $field, string $value ) : bool
    {
        $query = "UPDATE {$this->tableName()}
                     SET `{$field}` = :value
                   WHERE id = :id";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':value' => $value,
                ':id' => $id
            ]);
            return true;
        } catch (\PDOException $e) {
            echo "Error updating a color: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete( $id ) : bool
    {
        $query = "DELETE FROM {$this->tableName()} WHERE id = :id";

        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $id]);
            return true;
        } catch (\PDOException $e) {
            echo "Error deleting a color: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
}
