<?php

namespace App\Models;

use PDO;
use Core\Connection;

/**
 * This is the model class for table "cat-pattern".
 *
 * @property int $id
 * @property string $name
 * @property string $note
 *
 * @property CatPattern[] $getPatterns
 * @property string $getPattern
 * @property bool $create
 * @property bool $delete
 * @property bool $update
 */
class CatPattern
{
    private $id;
    private $name;
    private $note;

    private $db;

    public function __construct( ) {
        $this->db = Connection::openLink();
    }

    private function tableName() : string
    {
        return "cat_pattern";
    }

    /**
     * @return \App\Models\CatColor
     */
    public function getPatterns() : array
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute();

            return $cursor->fetchAll(PDO::FETCH_ASSOC);
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return string
     */
    public function getPattern($pattern_id) : string
    {
        $query = "SELECT name
                    FROM `" . self::tableName() . "`
                   WHERE `id` = :patternID";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':patternID' => $pattern_id]);

            return $cursor->fetchColumn();
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function create( ) : bool
    {
        $query = "INSERT INTO {$this->tableName()}
                              ( name, note )
                       VALUES ( :name, :note )";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':name' => $_POST['name'],
                ':note' => $_POST['note'],
            ]);
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            echo "Error creating a pattern: ", $e->getMessage(), "<br/>";
            die();
        }
    }
    /**
     * @return bool
     */
    public function update( int $id, string $field, string $value ) : bool
    {
        $query = "UPDATE {$this->tableName()}
                     SET `{$field}` = :value
                   WHERE id = :id";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':value' => $value,
                ':id' => $id
            ]);
            return true;
        } catch (\PDOException $e) {
            echo "Error updating a pattern: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete( $id ) : bool
    {
        $query = "DELETE FROM {$this->tableName()} WHERE id = :id";

        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $id]);
            return true;
        } catch (\PDOException $e) {
            echo "Error deleting a pattern: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
}
