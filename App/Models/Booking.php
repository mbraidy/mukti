<?php

namespace App\Models;

use DateTime;
use PDO;
use Core\Connection;

/**
 * This is the model class for table "cat"
 *
 * @property int $id
 * @property date $date
 * @property time $time
 * @property bool $booked
 *
 * @property Booking[] getBookings
 * @property Booking[] $find
 * @property bool $create
 * @property bool $delete
 * @property string $update
 */
class Booking
{
    private $db;

    public function __construct( ) {
        $this->db = Connection::openLink();
    }

    private function tableName() : string
    {
        return "booking";
    }

    /**
     * @return \App\Models\Booking
     */
    public function getBookings() : array
    {
        $query = "SELECT booking.*,
                         cat_race.name AS race,
                         cat_color.name AS color
                    FROM `" . self::tableName() . "`
                    JOIN user ON userID = user.id
                    JOIN calendar ON calendarID = calendar.id";
        if (!User::isAdmin()) {
            $query .= " WHERE booked = 0 AND available = 1";
        }

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute();
            $bookings = $cursor->fetchAll(PDO::FETCH_ASSOC);
            foreach ($cats as $index => $booking) {
                $bookings[$index]['dob'] = substr($booking['dob'], 0, 10);
            }
            return $bookings;
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return \App\Models\Booking
     */
    public function getFullSchedule() : array
    {
        $query = "SELECT booking.*,
                         user.id AS user_id, user.name AS adaptor,
                         email, phone, username,
                         cat_race.name AS race,
                         cat_color.name AS color,
                         cat.dob, cat.name, cat.id,
                         cat.picture, cat.gender,
                         cat.chip, cat.note
                    FROM `" . self::tableName() . "`
                    JOIN user ON userID = user.id
                    JOIN cat ON catID = cat.id
                    JOIN cat_race ON raceID = cat_race.id
                    JOIN cat_color ON colorID = cat_color.id
                   WHERE done = 0";
        if (!User::isAdmin()) {
            $query .= " AND userID = :userID";
        } else {

        }
        $query .= " ORDER BY date DESC, timing DESC";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':userID' => $_SESSION['ID']]);
            $bookings = $cursor->fetchAll(PDO::FETCH_ASSOC);
            foreach ($bookings as $index => $booking) {
                $diff = time() - strtotime($booking['dob']);
                $bookings[$index]['age'] = $diff / 3600/24;
                $bookings[$index]['dob'] = substr($booking['dob'], 0, 10);
            }
            return $bookings;
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
    /**
     * @return \App\Models\Booking
     */
    public function getHistory() : array
    {
        $query = "SELECT booking.*,
                         user.id AS user_id, user.name AS adaptor,
                         email, phone, username,
                         cat_race.name AS race,
                         cat_color.name AS color,
                         cat.dob, cat.name, cat.id,
                         cat.picture, cat.gender,
                         cat.chip, cat.note
                    FROM `" . self::tableName() . "`
                    JOIN user ON userID = user.id
                    JOIN cat ON catID = cat.id
                    JOIN cat_race ON raceID = cat_race.id
                    JOIN cat_color ON colorID = cat_color.id
                   WHERE done = 1
                ORDER BY date DESC, timing DESC";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':userID' => $_SESSION['ID']]);
            $bookings = $cursor->fetchAll(PDO::FETCH_ASSOC);
            foreach ($bookings as $index => $booking) {
                $diff = time() - strtotime($booking['dob']);
                $bookings[$index]['age'] = $diff / 3600/24;
                $bookings[$index]['dob'] = substr($booking['dob'], 0, 10);
            }
            return $bookings;
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function create() : bool
    {
        try {
            $queryCheck = "SELECT booking.id
                             FROM `" . self::tableName() . "`
                             JOIN cat ON cat.id = catID
                            WHERE catID = :catID
                              AND userID = :userID
                              AND done = 0
                              AND booked = 1
                              AND available = 1";
            $cursor = $this->db->prepare ( $queryCheck, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':catID' => $_POST['cat'],
                ':userID' => $_SESSION['ID']
            ]);
            $bookings = $cursor->fetch(PDO::FETCH_ASSOC);

            $this->db->beginTransaction();

            if (empty($bookings)) {
                $queryBooking = "INSERT INTO `" . $this->tableName() . "`
                                          ( `catID`, `userID`, `date`, `timing` )
                                   VALUES ( :catID, :userID, :date, :timing )";
                $queryCat = "UPDATE cat
                                SET booked = 1
                              WHERE id = :id";
                $cursor = $this->db->prepare ( $queryBooking, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
                $cursor->execute([
                    ':catID' => $_POST['cat'],
                    ':userID' => $_SESSION['ID'],
                    ':date' => $_POST['date'],
                    ':timing' => $_POST['timing']
                ]);

                $cursor = $this->db->prepare ( $queryCat, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
                $cursor->execute([
                    ':id' => $_POST['cat']
                ]);
            } else {
                $bookingID = $bookings['id'];
                $queryBooking = "UPDATE `" . $this->tableName() . "`
                                    SET date = :date,
                                        timing = :timing
                                  WHERE id = {$bookingID}";
                $cursor = $this->db->prepare ( $queryBooking, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
                $cursor->execute([
                    ':date' => $_POST['date'],
                    ':timing' => $_POST['timing']
                ]);
            }

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function adopt($date, $timing, $catID, $adopter) : bool
    {
        $queryCat = "UPDATE cat
                        SET available = 0, booked = 0
                      WHERE id = :id";
        $queryBooked = "UPDATE booking
                           SET done = 1, adopted = 1
                         WHERE date = :date
                           AND timing = :timing
                           AND catID = :catID
                           AND userID = :userID";
        try {

            $this->db->beginTransaction();

            $cursor = $this->db->prepare ( $queryBooked, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':catID' => $catID,
                ':userID' => $adopter,
                ':date' => $date,
                ':timing' => $timing
            ]);
            $cursor = $this->db->prepare ( $queryCat, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':id' => $catID
            ]);

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function noshow($date, $timing, $catID, $adopterID) : bool
    {
        $queryCat = "UPDATE cat
                        SET available = 1, booked = 0
                      WHERE id = :id";
        $queryBooked = "UPDATE booking
                           SET done = 1, noShow = 1
                         WHERE date = :date
                           AND timing = :timing
                           AND catID = :catID
                           AND userID = :userID";
        try {
            $this->db->beginTransaction();

            $cursor = $this->db->prepare ( $queryBooked, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':catID' => $catID,
                ':userID' => $adopterID,
                ':date' => $date,
                ':timing' => $timing
            ]);
            $cursor = $this->db->prepare ( $queryCat, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':id' => $catID
            ]);

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function nointerest($date, $timing, $catID, $adopterID) : bool
    {
        $queryCat = "UPDATE cat
                        SET available = 1, booked = 0
                      WHERE id = :id";
        $queryBooked = "UPDATE booking
                           SET done = 1, noInterest = 1
                         WHERE date = :date
                           AND timing = :timing
                           AND catID = :catID
                           AND userID = :userID";
        try {
            $this->db->beginTransaction();

            $cursor = $this->db->prepare ( $queryBooked, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':catID' => $catID,
                ':userID' => $adopterID,
                ':date' => $date,
                ':timing' => $timing
            ]);
            $cursor = $this->db->prepare ( $queryCat, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':id' => $catID
            ]);

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function update( int $id, string $field, string $value ) : bool
    {
        $query = "UPDATE `" . $this->tableName() . "`
                     SET `{$field}` = :value
                   WHERE id = :id";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':value' => $value,
                ':id' => $id
            ]);
            return true;
        } catch (\PDOException $e) {
            echo "Error updating a booking: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete( $date, $timing, $catId )
    {
        $params = [
            ':date' => $date,
            ':timing' => $timing,
            ':catID' => $catId
        ];
        $queryBooking = "DELETE
                           FROM `{$this->tableName()}`
                          WHERE `date` = :date
                            AND `timing` = :timing
                            AND `catID` = :catID";
        if (!User::isAdmin()) {
            $queryBooking .= " AND `userID` = :userID";
            $params[':userID'] = $_SESSION['ID'];
        }
        $queryCat = "UPDATE cat
                        SET booked = 0
                      WHERE id = :id";
        try {
            $this->db->beginTransaction();

            $cursor = $this->db->prepare ( $queryBooking, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute($params);

            $cursor = $this->db->prepare ( $queryCat, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $catId]);

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            echo "Error deleting a booking: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return \App\Models\Booking
     */
    public function find( string $date )
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`
                   WHERE date = :date
                     AND done = 0";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute( [ ':date' => $date] );
            $timings = $cursor->fetchAll(PDO::FETCH_ASSOC);
            $schedule = [];
            foreach ($timings as $timing) {
                if ($timing['userID'] == $_SESSION['ID']) {
                    $schedule['user'][] = $timing['timing'];
                } else {
                    $schedule['booked'][] = $timing['timing'];
                }
            }
            return $schedule;
        } catch ( \PDOException $e ) {
            echo "Error retrieving cats!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
}
