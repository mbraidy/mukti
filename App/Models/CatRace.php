<?php

namespace App\Models;

use PDO;
use Core\Connection;

/**
 * This is the model class for table "cat-race".
 *
 * @property int $id
 * @property string $name
 * @property string $note
 * @property string $picture1
 * @property string $picture2
 * @property string $playfulness
 *
 * @property CatRace[] $getRaces
 * @property string $getRace
 * @property bool $create
 * @property bool $delete
 * @property bool $update
 */
class CatRace
{
    private $id;
    private $name;
    private $note;
    private $picture1;
    private $picture2;
    private $playfulness;

    private $db;

    public function __construct( ) {
        $this->db = Connection::openLink();
    }

    private function tableName() : string
    {
        return "cat_race";
    }

    /**
     * @return \App\Models\CatRace
     */
    public function getRaces() : array
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute();

            return $cursor->fetchAll(PDO::FETCH_ASSOC);
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return string
     */
    public function getRace(int $race_id) : string
    {
        $query = "SELECT name
                    FROM `" . self::tableName() . "`
                   WHERE `id` = :raceID";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute([':raceID' =>  $race_id]);

            return $cursor->fetchColumn();
        } catch ( \PDOException $e ) {
            echo "Error retrieving race!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return \App\Models\CatColor
     */
    public function find(int $id) : array
    {
        $query = "SELECT *
                    FROM `" . self::tableName() . "`
                   WHERE id = :id";

        try {
            $cursor = $this->db->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);

            $cursor->execute( [':id' => $id] );

            return $cursor->fetch(PDO::FETCH_ASSOC);
        } catch ( \PDOException $e ) {
            echo "Error retrieving models!: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function create( ) : int
    {
        $query = "INSERT INTO `{$this->tableName()}`
                              ( `name`, `note`, `picture1`, `picture2`, `playfulness` )
                       VALUES ( :name, :note, :picture1, :picture2, :playfulness )";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':name' => $_POST['name'],
                ':note' => $_POST['note'],
                ':picture1' => $_POST['picture1'],
                ':picture2' => $_POST['picture2'],
                ':playfulness' => $_POST['playfulness']
            ]);
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            echo "Error creating a race: ", $e->getMessage(), "<br/>";
            die();
        }
    }
    /**
     * @return bool
     */
    public function update( int $id, string $field, string $value ) : bool
    {
        $query = "UPDATE `{$this->tableName()}`
                     SET `{$field}` = :value
                   WHERE id = :id";
        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([
                ':value' => $value,
                ':id' => $id
            ]);
            return true;
        } catch (\PDOException $e) {
            echo "Error updating a race: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete( $id ) : bool
    {
        $query = "DELETE FROM `{$this->tableName()}` WHERE `id` = :id";

        try {
            $cursor = $this->db->prepare ( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ] );
            $cursor->execute([':id' => $id]);
            return true;
        } catch (\PDOException $e) {
            echo "Error deleting a race: ", $e->getMessage(), "<br/>";
            die();
        }
        return false;
    }
}
