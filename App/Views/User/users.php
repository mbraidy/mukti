<?php
/**
 *  @var $data App\Models\User
 */
?>
<h3>Users list</h3>
</button>

<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/user/update'>
    <thead class="thead-dark">
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Role</th>
            <th>E-mail</th>
            <th>Phone</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody class="data-table">
    <?php foreach ( $data as $user ) { ?>
        <tr>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="name-<?=$user['id']?>">
                <?=$user['name']?>
            </td>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="username-<?=$user['id']?>">
                <?=$user['username']?>
            </td>
            <td><?=$user['role']?></td>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="email-<?=$user['id']?>">
                <?=$user['email']?>
            </td>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="phone-<?=$user['id']?>">
                <?=$user['phone']?>
            </td>

            <td nowrap>
                <?php if ($user['username'] == "Admin") { ?>
                          &ensp;
                         <i title='You cannot delete Admin'
                              class="far fa-trash-alt text-link"></i>
                         &ensp;
               <?php } else { ?>
                         <button type="button" onclick="delete_row(this.id)"
                                 data-ref='/user/delete'
                                 id='del-<?=$user['id']?>'
                                 title='Delete <?=$user['username']?>'
                                 class="delete_row btn btn-link">
                               <i class="far fa-trash-alt text-danger"></i>
                         </button>
               <?php } ?>
                         <button type="button" onclick="change_password(this.id)"
                                 data-ref='/user/change_password'
                                 id='update-<?=$user['id']?>'
                                 title='Edit <?=$user['username']?>'
                                 class="update_row btn btn-link">
                               <i class="fa fa-key text-primary"></i>
                         </button>
            </td>
        </tr>
    <?php } ?>
   </tbody>
</table>