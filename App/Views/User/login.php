<section>
  <form action="./login" method="POST" id="loginForm">
  	<?php if (isset($waitTime) && $waitTime>0) { ?>
  		<div data-seconds=<?=$waitTime?> id="login-time"></div>
  	<?php } ?>
  	<div id="login-time"></div>

    <div>
        <div class="hide-md-lg">
          <p>Sign in using username/password:</p>
        </div>

        <input type="text" name="username" placeholder="Username" required>
        <div class="input-group mb-3">
            <input type="password" placeholder="Password" required
                   id="password" name="password" class="form-control col-3"
                   title="Enter your registered password">
            <div class="input-group-append" id="hide-show-passwword">
                <span class="input-group-text">
                    <i class="far fa-eye-slash fa-2x text-success"></i>
                </span>
            </div>
        </div>
        <input type="submit" value="submit" class="loginbtn">
        <a href="/user/forgotPassword" style="color:white" class="btn">Forgot password?</a>
    </div>
  </form>
</section>
