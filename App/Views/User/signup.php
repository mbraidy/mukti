<div class="container">
  <form action="/user/signup" method="POST" id="signupForm">
  	<div class="row">
  		<div class="col-sm-5">
      	    <label for="username">Username</label>
        	<input type="text" placeholder="user" id="username" name="username" required>
 			<br/><br/>
			<label for="password">Password</label>
			<br/>
		  	<div class="row">
				<div class="col-sm-10">
                     <input type="password" placeholder="Password" required
                  		 id="password" name="password" class="signup"
    					 pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
    					 title="Must contain at least 1 number, 1 uppercase, 1 lowercase letter and at least 8 characters">
				</div>
				<div class="col-sm-2" id="hide-show-passwword"><i class="far fa-eye-slash fa-2x text-success"></i></div>
            </div>

			<br/>
            <label for="password-repeat"><b>Repeat Password</b></label>
            <input type="password" placeholder="Repeat Password" required
            	   id="password-repeat" name="password-repeat" class="signup"
            	   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
            	   title="Must contain at least 1 number, 1 uppercase, 1 lowercase letter and at least 8 characters">
    	</div>
  	</div>

    <p>By creating an account you agree to our<button type="button" class="btn btn-link" data-toggle="modal" data-target="#genericModal">
          Terms & Conditions</button>.</p>
    <p hidden class="text-success" id="terms_accept" >You have agreed to the terms and conditions.</p>


    <div class="clearfix">
      <button type="button" class="cancelbtn"><a href="/user/login">Cancel</a></button>
      <button type="button" class="signupbtn" onclick="signup()">Sign Up</button>
    </div>
  </form>
</div>

