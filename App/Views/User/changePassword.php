<section class="grid-container">

    <label for="oldpass" class="grid-col-1"><b>Old password</b></label>
    <input type="text" class="grid-col-2"
           placeholder="Enter Old password" id="oldpass" required>

    <label for="password" class="grid-col-1"><b>Password</b></label>
    <input type="password" class="grid-col-2"
           placeholder="Enter new Password" id="password" required>

    <label for="pswRepeat" class="grid-col-1"><b>Repeat Password</b></label>
    <input type="password" class="grid-col-2"
           placeholder="Repeat the new Password" id="pswRepeat" required>

    <button type="button" id="registerbtn"
            onclick="change_password()">
         Change password
    </button>
</section>
