<?php
/* @var $data App\Models\User */
?>
<form action="/User/profile" method="POST" id="profileForm">
    <div class="grid-container" id="user_profile">

        <h2 class="grid-header"><?= $data['username']?>'s Profile:</h2>

        <label for="name" class="grid-col-2">Name:</label>
        <input type="text" id="name" name="name" required
               class="grid-col-3 updatable_field"
               maxlength="50" size="25" value="<?= $data['name']?>">

        <label for="username" class="grid-col-2">Username:</label>
        <input type="text" id="username" name="username" required
               class="grid-col-3 updatable_field"
               maxlength="30" size="25" value="<?= $data['username']?>">

        <label class="grid-col-2">Role:</label>
        <input type="text" id="role" name="role" required
               class="grid-col-3 updatable_field" disabled
               size="25" value="<?= $data['role']?>">

        <h3 class="grid-header">Contact</h3>

        <label for="email" class="grid-col-2">E-mail:</label>
        <input type="text" id="email" name="email" required
               class="grid-col-3 updatable_field"
               maxlength="100" size="25" value="<?= $data['email']?>">

        <label for="phone" class="grid-col-2">Telephone:</label>
        <input type="text" id="phone" name="phone" required
               class="grid-col-3 updatable_field"
               maxlength="30" size="25" value="<?= $data['phone']?>">
   </div>
</form>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#genericModal">
  Change Password
</button>
