<?php
/* @var $data App\Models\CatRace */
?>
<input type="hidden" id="imageLocation" value="/Assets/images/race" />

<h3>Cats' races&emsp;
    <button type="button" class="btn btn-success" onclick="create_new()" data-target="#genericModal">
        <i class="fas fa-plus fa-1x text-primary"></i>
    </button>
</h3>
<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/cat/updaterace' data-view='/cat/viewrace' data-loc="/Assets/images/race/">
    <thead class="thead-dark">
        <tr>
            <th>Race</th>
            <th>Playfulness</th>
            <th>Picture</th>
            <th>Picture</th>
            <th>Actions</th>
        </tr>
    </thead>
    <?php foreach ( $data as $race ) { ?>
        <tbody id="body-grid" class="data-table">
        <tr class='parent'>
            <td contenteditable="true" onblur="update_row(this.id, 'child')"
                                       id="parent_name-<?=$race['id']?>">
                <?=$race['name']?>
            </td>
            <td contenteditable="true" onblur="update_row(this.id, 'child')"
                                       id="parent_playfulness-<?=$race['id']?>">
                <?=$race['playfulness']?>
            </td>
            <td>
                <?php if ( empty($race['picture1']) ) {?>
                         <img src="" alt="No image yet" height="36"
                              id="parent_picture1-<?=$race['id']?>"/>
                <?php } else {?>
                        <img src="/Assets/images/race/<?=$race['picture1']?>"
                             class="zoom" alt="No image yet" height="36"
                             id="parent_picture1-<?=$race['id']?>"/>
                <?php } ?>
            </td>
            <td>
                <?php if ( empty($race['picture2']) ) {?>
                         <img src="" alt="No image yet" height="36"
                              id="parent_picture2-<?=$race['id']?>"/>
                <?php } else {?>
                        <img src="/Assets/images/race/<?=$race['picture2']?>"
                             class="zoom" alt="No image yet" height="36"
                             id="parent_picture2-<?=$race['id']?>"/>
                <?php } ?>
            </td>
            <td nowrap>
               <button type="button" onclick="delete_row(this.id)"
            	       data-ref='/cat/delrace'
                       id='del-<?=$race['id']?>'
                       class="delete_row btn btn-link">
                     <i class="far fa-trash-alt text-danger"></i>
               </button>
               <button type="button"
                       data-ref='/cat/updaterace'
                       id='update-<?=$race['id']?>'
                       class="update_row btn btn-link">
                     <i class="far fa-eye text-primary"></i>
               </button>
            </td>
        </tr>
        <tr class='child'>
            <td colspan=5>
                <section class="editrace_container">
                    <label for="name" class="race-head-left"><b>Race</b></label>
                    <label for="playfulness" class="race-head-right"><b>Playfulness</b></label>
                    <input type="text" class="race-input-left input-field" name="name"
                           value="<?=$race['name']?>"  onblur='update_row(this.id, "parent")'
                           placeholder="Enter a race" id="name-<?=$race['id']?>" required>
                    <div class="race-input-right">
                        <input type="range" min="1" max="10" value="<?=$race['playfulness']?>"
                               class="input-field" name="playfulness"
                               placeholder="Enter playfulness" id="playfulness-<?=$race['id']?>"
                               oninput="playfulnessUpdate(this.id, value, 'parent')"/>
                        <output for="playfulness" id="playfulnessOutput-<?=$race['id']?>">
                        <?=$race['playfulness']?>
                        </output>
                    </div>
                    <label for="note" class="race-head-left"><b>Notes</b></label>
                    <textarea class="race-input-wide input-field" name="note"  onblur="update_row(this.id)"
                              placeholder="Enter notes" id="note-<?=$race['id']?>"
                              rows="3" cols="30"><?=$race['note']?></textarea>
                    <br/>
                    <label class="race-head-left"><b>Pictures</b></label>
                    <img src="/Assets/images/race/<?=$race['picture1']?>"
                         id="show-picture1-<?=$race['id']?>" class="race-input-left"
                         alt="No image yet" height="100" />
                    <img src="/Assets/images/race/<?=$race['picture2']?>"
                         id="show-picture2-<?=$race['id']?>" class="race-input-right"
                         alt="No image yet" height="100" />

                   <output for="show-picture1-<?=$race['id']?>"
                           class="race-input-left" id="output-picture1-<?=$race['id']?>">
                       <?=$race['picture2']?>
                   </output>
                   <output for="show-picture1-<?=$race['id']?>"
                           class="race-input-right" id="output-picture2-<?=$race['id']?>">
                       <?=$race['picture2']?>
                   </output>

                   <input type="file" id="picture1-<?=$race['id']?>" name="picture1"
                          class="race-input-left input-field" onchange="handleFileSelect(this, 'parent')"/>
                   <input type="file" id="picture2-<?=$race['id']?>" name="picture2"
                          class="race-input-right input-field" onchange="handleFileSelect(this, 'parent')"/>
                </section>
            </td>
        </tr>
        </tbody>
    <?php } ?>
</table>