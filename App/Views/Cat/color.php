<?php
/* @var $data App\Models\CatColor */
?>
<h3>Cats' colors&emsp;
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#genericModal">
  <i class="fas fa-plus fa-1x text-primary"></i>
</button>

</h3>
<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/cat/updatecolor'>
    <thead class="thead-dark">
        <tr>
            <th>Color</th>
            <th>Note</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="body-grid">
    <?php foreach ( $data as $color ) { ?>
        <tr>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="name-<?=$color['id']?>">
                <?=$color['name']?>
            </td>
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="note-<?=$color['id']?>">
                <?=$color['note']?>
            </td>
            <td>
                <button type="button" onclick="delete_row(this.id)"
            	        data-ref='/cat/delcolor'
                        id='del-<?=$color['id']?>'
                        class="delete_row btn btn-link">
                     <i class="far fa-trash-alt text-danger"></i>
                </button>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>