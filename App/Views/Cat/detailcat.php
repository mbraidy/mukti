<section class="cat-container">
    <label for="name" class="cat-head-11"><b>Name</b></label>
    <label for="race" class="cat-head-12"><b>Race</b></label>
    <input type="text" class="noedit cat-input-11" id="name"/>
    <input type="text" class="noedit cat-input-12" id="race"/>

    <label for="dob" class="cat-head-21"><b>DoB</b><span id="age"></span></label>
    <label for="color" class="cat-head-22"><b>Color</b></label>
    <label for="gender" class="cat-head-23"><b>Gender</b></label>
    <input type="text" class="noedit cat-input-21" id="dob"/>
    <input type="text" class="noedit cat-input-22" id="color"/>
    <input type="text" class="noedit cat-input-23" id="gender"/>

    <label for="chip" class="cat-head-31"><b>Chip number</b></label>
    <input type="text" class="noedit cat-input-31" id="chip"/>
    <img src="" id="picture" class="cat-input-32"
         alt="No image yet" height="50"/>
    <div class="cat-input-33" id="booked"></div>

    <label for="note" class="cat-head-41"><b>Notes</b></label>
    <textarea class="noedit cat-input-41" id="note"></textarea>

    <details class="cat-head-51">
      <summary  class="cat-head-61">Race details</summary>
      <section class="noedit cat-input-61 scroll" id="details"></section>
    </details>

</section>
