<form id="embedded-form" method="POST" action='/cat/updaterace'>
    <section class="grid-container">
        <label for="name" class="grid-col-2"><b>Race</b></label>
         <p contenteditable="true" onblur="update_row(this.id)" id="name"></p>

        <label for="note" class="grid-col-2"><b>Note</b></label>
         <p contenteditable="true" onblur="update_row(this.id)" id="note"></p>

        <br/>
        <label for="picture1" class="grid-col-2"><b>Picture</b></label>
        <img id="picture1" src="" alt="image" />

        <label for="picture2" class="grid-col-2"><b>Picture</b></label>
        <img id="picture2" src="" alt="image" />

        <label for="playfulness" class="grid-col-2"><b>Playfulness</b></label>
        <input id="playfulness" value="5" />

        <br/>

        <button type="button" id="registerbtn"
                onclick="add_entity()">
             commit
        </button>
    </section>
</form>
