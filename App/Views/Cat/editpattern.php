<form id="embedded-form" method="POST" action='/cat/pattern'>
    <section class="grid-container">
        <label for="name" class="grid-col-2"><b>Pattern</b></label>
        <input type="text" class="grid-col-3 input-field" name="name"
               placeholder="Enter a pattern" id="name" required>

        <label for="note" class="grid-col-2"><b>Note</b></label>
        <input type="text" class="grid-col-3 input-field" name="note"
               placeholder="Enter a note" id="note">

        <br/>
        <button type="button" id="registerbtn"
                onclick="add_entity()">
             commit
        </button>
    </section>
</form>
