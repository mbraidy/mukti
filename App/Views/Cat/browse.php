<?php
/* @var $data App\Models\Cat */
?>
<input type="hidden" id="imageLocation" value="/Assets/images/cat" />
<h3>Available cats</h3>
<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/cat/updatecat' data-view='/cat/viewcat' data-image="/Assets/images/cat/">
    <thead class="thead-dark">
        <tr>
            <th>Name</th>
            <th>Race</th>
            <th>Picture</th>
            <th>Booked</th>
            <th>Gender</th>
            <th>Actions</th>
        </tr>
    </thead>
    <?php foreach ( $data as $cat) { ?>
        <tbody class="data-table">
        <tr class="parent">
            <td><?=$cat['name']?></td>
            <td><?=$cat['race']?></td>
            <td>
                <?php if ( empty($cat['picture']) ) {?>
                         <img src="" alt="No image yet" height="36"
                              id="picture-<?=$cat['id']?>"/>
                <?php } else {?>
                        <img src="/Assets/images/cat/<?=$cat['picture']?>"
                             class="zoom" alt="No image yet" height="36"
                             id="picture-<?=$cat['id']?>"/>
                <?php } ?>
            </td>
            <td class="text-center">
               <i class="far <?= ($cat['booked'])? 'fa-calendar-check text-success' : 'fa-calendar-times text-danger'?>"></i>
           </td>
            <td><?= $cat['gender'] ?></td>
            <td nowrap>
               <button type="button"
                       data-ref='/cat/updatecat'
                       id='view-<?=$cat['id']?>'
                       class="btn btn-link"
                       title="View cat's details">
                     <i class="far fa-eye text-primary"></i>
               </button>
               <button type="button"
                       data-ref='/booking/book'
                       id='book-<?=$cat['id']?>'
                       class="btn btn-link"
                       onclick="bookTime(this.id)"
                       title="Book an appointment to meet the cat">
                     <i class="far fa-calendar-plus text-success"></i>
               </button>
            </td>
        </tr>
        <tr class="child">
            <td colspan=7>
              <section class="editcat-container">
                <label for="name" class="cat-head-left"><b>Cat</b></label>
                <label for="raceID" class="cat-head-right"><b>Race</b></label>
                <p class="cat-input-left"><?=$cat['name']?></p>
                <p class="cat-input-right"><?=$cat['race']?></p>

                <label for="dob-<?=$cat['id']?>" class="cat-head-left"><b>DoB</b><span id="age-<?=$cat['id']?>"></span></span></label>
                <label for="colorID" class="cat-head-right"><b>Color</b></label>
                <p class="cat-input-left"><?=$cat['dob']?></p>
                <p class="cat-input-right"><?=$cat['color']?></p>

                <label for="chip" class="cat-head-left"><b>Chip number</b></label>
                <label for="gender" class="cat-head-right"><b>Gender</b></label>
                <p class="cat-input-left"><?=$cat['chip']?></p>
                <p class="cat-input-right"><?=$cat['gender']?></p>

                <label for="note" class="cat-head-left"><b>Notes</b></label>
                <textarea class="cat-input-wide" disabled><?=$cat['note']?></textarea>

                <br/>
                <label class="cat-head-left"><b>Pictures</b></label>
                <label class="cat-head-right2 text-nowrap"><b><?=$cat['booked']?'Booked':'Not booked'?></b></label>

                <img src='/Assets/images/cat/<?=$cat['picture']?>'
                     class="cat-input-left" alt="No image" height="100"/>
                <?php if ( $cat['booked'] ) { ?>
                         <button type="button" onclick="book(this.id)"
                                 data-ref='/booking/book'
                                 id='re_book-<?=$cat['id']?>'
                                 class="btn btn-link">
                                 <i class="far fa-calendar-plus fa-3x text-success"></i>
                         </button>
                <?php } else { ?>
                         <button type="button"
                                 data-ref='/booking/book'
                                 id='schedule-<?=$cat['id']?>'
                                 class="btn btn-link"
                                 onclick="bookTime(this.id)">
                                 <i class="far fa-calendar-plus fa-3x text-danger"></i>
                         </button>
                <?php } ?>

                <?= $cat['booked']? "checked" : ""?>

              </section>
            </td>
        </tr>
        </tbody>
    <?php } ?>
</table>
