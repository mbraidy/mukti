<?php
/* @var $data App\Models\Cat */
?>
<input type="hidden" id="imageLocation" value="/Assets/images/cat" />
<h3>Available cats&emsp;<button type="button" class="btn btn-success" onclick="create_new()">
                            <i class="fas fa-plus fa-1x text-primary"></i>
                        </button>
</h3>
<table class="table w-100 table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-book='/booking/book' data-update='/cat/updatecat' data-view='/cat/viewcat' data-image="/Assets/images/cat/">
    <thead class="thead-dark">
        <tr>
            <th>Name</th>
            <th>Race</th>
            <th>Picture</th>
            <th>Available</th>
            <th>Booked</th>
            <th>Gender</th>
            <th>Actions</th>
        </tr>
    </thead>

    <?php foreach ( $data as $cat) { ?>
         <tbody class="data-table" id="tbody-<?=$cat['id']?>">
         <tr class="parent">
            <td contenteditable="true" onblur="update_row(this.id)"
                                       id="parent_name-<?=$cat['id']?>">
                <?=$cat['name']?>
            </td>
            <td id="parent_raceID-<?=$cat['id']?>"><?=$cat['race']?></td>
            <td>
                <?php if ( empty($cat['picture']) ) {?>
                         <img src="" alt="No image yet" height="36"
                              id="parent_picture-<?=$cat['id']?>"/>
                <?php } else {?>
                        <img src="/Assets/images/cat/<?=$cat['picture']?>"
                             class="zoom" alt="No image yet" height="36"
                             id="parent_picture-<?=$cat['id']?>"/>
                <?php } ?>
            </td>
            <td class="text-center">
               <i id="parent_available-<?=$cat['id']?>"
                  class="far <?= ($cat['available'])? 'fa-check-circle text-success' : 'fa-times-circle text-danger'?>"></i>
            </td>
            <td class="text-center">
               <i id="parent_booked-<?=$cat['id']?>"
                  class="far <?= ($cat['booked'])? 'fa-calendar-check text-success' : 'fa-calendar-times text-danger'?>"></i>
           </td>
            <td id="parent_gender-<?=$cat['id']?>">
               <?= $cat['gender'] ?>
            </td>
            <td nowrap>
               <button type="button" onclick="delete_row(this.id)"
            	       data-ref='/cat/delcat'
                       id='del-<?=$cat['id']?>'
                       class="delete_row btn btn-link">
                     <i class="far fa-trash-alt text-danger"></i>
               </button>
               <button type="button"
                       data-ref='/cat/updatecat'
                       id='update-<?=$cat['id']?>'
                       class="update_row btn btn-link">
                     <i class="far fa-eye text-primary"></i>
               </button>
            </td>
        </tr>
        <tr class="child">
            <td colspan=7>
              <section class="editcat-container">
                <label for="name" class="cat-head-left"><b>Cat</b></label>
                <label for="raceID" class="cat-head-right"><b>Race</b></label>
                <input type="text" class="cat-input-left input-field" name="name"
                       onblur="update_row(this.id, 'parent')"
                       placeholder="Enter the cat's name" id="name-<?=$cat['id']?>" required
                       value='<?=$cat['name']?>'/>
                <select class="cat-input-right input-field"
                        onchange="update_row(this.id, 'parent')"
                        name="raceID" id="raceID-<?=$cat['id']?>">
                   <option value='<?=$cat['raceID']?>' disabled>Select the race...</option>
                   <?php foreach ( $races as $race ) {
                             echo "<option value='{$race['id']}'" .
                                        ($cat['raceID'] == $race['id'] ? "selected>" : ">") .
                                    $race['name'] .
                                  "</option>";
                         } ?>
                </select>

                <label for="dob-<?=$cat['id']?>" class="cat-head-left"><b>DoB</b><span id="age-<?=$cat['id']?>"></span></span></label>
                <label for="colorID" class="cat-head-right"><b>Color</b></label>
                <input type="date" class="cat-input-left input-field" name="dob"
                       placeholder="Date of birth" id="dob-<?=$cat['id']?>" required
                       onchange="update_row(this.id)"
                       min="2010-01-01" max=<?=date('Y-m-d')?> value='<?=$cat['dob']?>'/>
                <select class="cat-input-right input-field"
                        onchange="update_row(this.id)"
                        name="colorID" id="colorID-<?=$cat['id']?>">
                   <option value='' disabled>Select the color...</option>
                   <?php foreach ( $colors as $color ) {
                             echo "<option value='{$color['id']}'" .
                                        ($cat['colorID'] == $color['id'] ? "selected>" : ">") .
                                    $color['name'] .
                                  "</option>";
                          } ?>
                </select>

                <label for="chip" class="cat-head-left"><b>Chip number</b></label>
                <label for="gender" class="cat-head-right"><b>Gender</b></label>
                <input type="text" class="cat-input-left input-field" name="chip"
                       onchange="update_row(this.id)"
                       placeholder="Enter the cat's chip number" id="chip-<?=$cat['id']?>" required
                       value='<?=$cat['chip']?>'/>
                <select class="cat-input-right input-field"
                        onchange="update_row(this.id, 'parent')"
                        name="gender" id="gender-<?=$cat['id']?>">
                    <option value="" disabled>Select the gender...</option>
                    <?php if ( $cat['gender'] == 'male' ) { ?>
                             <option value='female'>Female</option>
                             <option value='male' selected>Male</option>
                    <?php } else { ?>
                             <option value='female' selected>Female</option>
                             <option value='male'>Male</option>
                    <?php } ?>
                </select>

                <label for="note" class="cat-head-left"><b>Notes</b></label>
                <textarea class="cat-input-wide input-field" name="note"
                          placeholder="Enter notes" id="note-<?=$cat['id']?>" rows="5" cols="50"></textarea>

                <br/>
                <label class="cat-head-left"><b>Pictures</b></label>
                <label class="cat-head-right1"><b>Available</b></label>
                <label class="cat-head-right2"><b>Booked</b></label>
                <input type="file" id="picture-<?=$cat['id']?>" name="picture"
                       class="cat-input-left input-field"
                       onchange="handleFileSelect(this, 'parent')"/>
                <output for="show-picture-<?=$cat['id']?>" class="cat-input-right"
                        id="output-picture-<?=$cat['id']?>"><?=$cat['picture']?></output>

                <img src='/Assets/images/cat/<?=$cat['picture']?>' id="show-picture-<?=$cat['id']?>"
                     class="cat-input-left" alt="No image yet" height="100"/>

                <div class="material-switch cat-input-right1">
                    <input type="checkbox" class='checkbox-hide input-field'
                           <?= $cat['available']? "checked" : ""?>
                           onchange="update_row(this.id, 'parent')"
                           name="available" id="available-<?=$cat['id']?>">
                    <label for="available-<?=$cat['id']?>" class="label-success"></label>
                </div>
                <div class="material-switch cat-input-right2">
                    <input type="checkbox" class='checkbox-hide input-field'
                           <?= $cat['booked']? "checked" : ""?>
                           onchange="update_row(this.id, 'parent')"
                           name="booked" id="booked-<?=$cat['id']?>">
                    <label for="booked-<?=$cat['id']?>" class="label-success"></label>
                </div>
                <br/>

              </section>
            </td>
        </tr>
        </tbody>
    <?php } ?>
</table>