<form id="embedded-form" method="POST" action='/cat/race'>
    <section class="race-container">
        <label for="name" class="race-head-left"><b>Race</b></label>
        <label for="playfulness" class="race-head-right"><b>Playfulness</b></label>
        <input type="text" class="race-input-left input-field" name="name"
               placeholder="Enter a race" id="name" required>
        <div class="race-input-right">
            <input type="range" min="1" max="10" value="3"
                   class="input-field" name="playfulness"
                   placeholder="Enter playfulness" id="playfulness"
                   oninput="playfulnessUpdate(this.id, value)">
            <output for="playfulness" id="playfulnessOutput">3</output>
        </div>
        <label for="note" class="race-head-left"><b>Notes</b></label>
        <textarea class="race-input-wide input-field" name="note"
                  placeholder="Enter notes" id="note" rows="5" cols="50"></textarea>

        <br/>
        <label class="race-head-left"><b>Pictures</b></label>
        <img src="" id="show-picture1" class="race-input-left"
             alt="No image yet" height="100" />
        <img src="" id="show-picture2" class="race-input-right"
             alt="No image yet" height="100" />

        <input type="file" id="picture1" name="picture1"
               class="race-input-left input-field" onchange="handleFileSelect(this)"/>
        <input type="file" id="picture2" name="picture2"
               class="race-input-right input-field" onchange="handleFileSelect(this)"/>

        <br/>
        <button type="button" id="registerbtn"
                onclick="add_race()">
             commit
        </button>
    </section>
</form>
