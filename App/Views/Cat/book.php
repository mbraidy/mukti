
<form id="embedded-form" method="POST" action='/cat/book'>
    <section class="cat-container">
        <label for="name" class="cat-head-left"><b>Cat</b></label>
        <label for="raceID" class="cat-head-right"><b>Race</b></label>
        <input type="text" class="cat-input-left input-field" name="name"
               placeholder="Enter the cat's name" id="name" required>
        <select class="cat-input-right input-field" name="raceID" id="raceID">
           <option value="" disabled selected>Select the race...</option>
           <?php foreach ( $races as $race ) { ?>
                  <option value='<?=$race['id']?>'><?=$race['name']?></option>
           <?php } ?>
        </select>

        <label for="dob" class="cat-head-left"><b>DoB</b><span id="age"></span></span></label>
        <label for="colorID" class="cat-head-right"><b>Color</b></label>
        <input type="date" class="cat-input-left input-field" name="dob"
               placeholder="Date of birth" id="dob" required onchange="update_age(this.value)"
               min="2010-01-01" max=<?=date('Y-m-d')?> />
        <select class="cat-input-right input-field" name="colorID" id="colorID">
           <option value="" disabled selected>Select the color...</option>
           <?php foreach ( $colors as $color ) { ?>
                  <option value='<?=$color['id']?>'><?=$color['name']?></option>
           <?php } ?>
        </select>

        <label for="chip" class="cat-head-left"><b>Chip number</b></label>
        <label for="gender" class="cat-head-right"><b>Gender</b></label>
        <input type="text" class="cat-input-left input-field" name="chip"
               placeholder="Enter the cat's chip number" id="chip" required/>
        <select class="cat-input-right input-field" name="gender" id="gender">
            <option value="" disabled selected>Select the gender...</option>
            <option value='female'>Female</option>
            <option value='male'>Male</option>
        </select>

        <label for="note" class="cat-head-left"><b>Notes</b></label>
        <textarea class="cat-input-wide input-field" name="note"
                  placeholder="Enter notes" id="note" rows="5" cols="50"></textarea>

        <br/>
        <label class="cat-head-left"><b>Pictures</b></label>
        <label class="cat-head-right1"><b>Booked</b></label>
        <label class="cat-head-right2"><b>Available</b></label>

        <img src="" id="show-picture" class="cat-input-left"
             alt="No image yet" height="100"/>

        <div class="material-switch cat-input-right1">
            <input type="checkbox" class='checkbox-hide input-field' name="booked" id="booked">
            <label for="booked" class="label-success"></label>
        </div>
        <div class="material-switch cat-input-right2">
            <input type="checkbox" class='checkbox-hide input-field' name="available" id="available">
            <label for="available" class="label-success"></label>
        </div>
        <br/>

        <input type="file" id="picture" name="picture"
               class="cat-input-left input-field" onchange="handleFileSelect(this)"/>
        <button type="button" id="registerbtn" class="cat-input-right btn-success"
                onclick="add_entity()">
             commit
        </button>
    </section>
</form>
