<header class="cd-main-header text-center flex flex-column flex-center">
  <p class="margin-top-md margin-bottom-xl"></p>
</header>
<section class="book-container">
    <label for="name" class="cat-head-11"><b>Name</b></label>
    <label for="race" class="cat-head-12"><b>Race</b></label>
    <p class="cat-input-11" id="cat-name"></p>
    <p class="cat-input-12" id="cat-race"></p>

    <label for="dob" class="cat-head-21"><b>DoB</b><span id="age"></span></label>
    <label for="color" class="cat-head-22"><b>Color</b></label>
    <label for="gender" class="cat-head-23"><b>Gender</b></label>
    <p class="cat-input-21" id="cat-dob"></p>
    <p class="cat-input-22" id="cat-color"></p>
    <p class="cat-input-23" id="cat-gender"></p>

    <label for="chip" class="cat-head-31"><b>Chip number</b></label>
    <p class="cat-input-31" id="cat-chip"/></p>
    <img src="" id="cat-picture" class="cat-input-32 zoom"
         alt="No image yet" height="50"/>
    <div class="cat-input-33" id="booked"></div>

    <label for="note" class="cat-head-41"><b>Notes</b></label>
    <p class="cat-input-41" id="cat-note"></p>
    <details class="cat-input-71">
        <summary class="">Race details</summary>
        <h4 id="race-name"></h4>
        <img src="" id="race-picture1" class="zoom"
             alt="No image yet" height="50"/>
        <img src="" id="race-picture2" class="zoom"
             alt="No image yet" height="50"/>
        <section class="noedit scroll" id="race-note"></section>
    </details>
    <main class="cat-input-72" >
        <nav class="book-appointment">
            <label for="book-Appointment" class="book-date-label">Select a date</label>
            <button type="button"
                   data-cat='none'
                   id='book-appointment'
                   class="btn btn-link book-date-action"
                   title="Confirm booking date and time for this cat"
                   onclick="bookAppointment()">
                 <i class="far fa-save fa-2x text-success"></i>
            </button>

            <input type="date" name="meeting" id="meeting_date"
                   min="<?=date('Y-m-d')?>"
                   class="book-date-select"
                   onchange="bookSelectDay(this.value)"/>
        </nav>
        <div id ="schedule-time" class="radio_container hide-calendar">
           <ul class="radio_list">
            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-08" />
             <label for="book-08" class="radio_label">08:00</label>
            </li>
            
            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-09" />
             <label for="book-09" class="radio_label">09:00</label>
            </li>
            
            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-10" />
             <label for="book-10" class="radio_label">10:00</label>
            </li>
            
            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-11" />
             <label for="book-11" class="radio_label">11:00</label>
            </li>

            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-12" />
             <label for="book-12" class="radio_label">12:00</label>
            </li>

            <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-13" />
             <label for="book-13" class="radio_label">13:00</label>
            </li>
 
             <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-14" />
             <label for="book-14" class="radio_label">14:00</label>
            </li>
 
             <li class="list__item">
             <input type="radio" class="radio_btn" name="timing" id="book-15" />
             <label for="book-15" class="radio_label">15:00</label>
            </li>
           </ul>
       </div>

    </main>
</section>
