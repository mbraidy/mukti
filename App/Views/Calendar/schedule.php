<?php
/* @var $data App\Models\Cat */
?>
<input type="hidden" id="imageLocation" value="/Assets/images/cat" />
<h3>Your Schedule</h3>
<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/cat/updatecat' data-view='/cat/viewcat' data-image="/Assets/images/cat/">
    <thead class="thead-dark">
        <tr>
            <th>Date</th>
            <th>Time</th>
            <th>Name</th>
            <th>Race</th>
            <th>Gender</th>
            <th>Actions</th>
        </tr>
    </thead>
    <?php foreach ( $schedule as $booking ) { ?>
        <tbody class="data-table" id='row-<?=$booking['id']?>'
               data-catid="<?=$booking['catID']?>"
               data-date="<?=$booking['date']?>"
               data-timing="<?=$booking['timing']?>">
        <tr class="parent">
            <td><?=$booking['date']?></td>
            <td><?=$booking['timing']?></td>
            <td><?=$booking['name']?></td>
            <td><?=$booking['race']?></td>
            <td><?=$booking['gender']?></td>
            <td nowrap>
               <button type="button"
                       id='view-<?=$booking['id']?>'
                       class="btn btn-link"
                       title="View cat's details">
                     <i class="far fa-eye text-primary"></i>
               </button>
               <button type="button"
                       data-ref='/booking/unbook'
                       id='book-<?=$booking['id']?>'
                       class="btn btn-link"
                       onclick="deleteTiming(this.id)"
                       title="Unbook the appointment">
                     <i class="far fa-calendar-plus text-danger"></i>
               </button>
            </td>
        </tr>
        <tr class="child">
            <td colspan=7>
              <section class="editcat-container">
                <label class="cat-head-left"><b>Cat</b></label>
                <label class="cat-head-right"><b>Race</b></label>
                <p class="cat-input-left"><?=$booking['name']?></p>
                <p class="cat-input-right"><?=$booking['race']?></p>

                <label class="cat-head-left"><b>DoB</b><span id="age-<?=$booking['id']?>"></span></span></label>
                <label class="cat-head-right"><b>Color</b></label>
                <p class="cat-input-left"><?=$booking['dob']?> (<?=(int)$booking['age']?> days)</p>
                <p class="cat-input-right"><?=$booking['color']?></p>

                <label class="cat-head-left"><b>Chip number</b></label>
                <label class="cat-head-right"><b>Gender</b></label>
                <p class="cat-input-left"><?=$booking['chip']?></p>
                <p class="cat-input-right"><?=$booking['gender']?></p>

                <label class="cat-head-left"><b>Notes</b></label>
                <textarea class="cat-input-wide" disabled><?=$booking['note']?></textarea>

                <br/>
                <label class="cat-head-left"><b>Pictures of <?=$booking['name']?></b></label>


                <img src='/Assets/images/cat/<?=$booking['picture']?>'
                     class="cat-input-left" alt="No image" height="100"/>

              </section>
            </td>
        </tr>
        </tbody>
    <?php } ?>
</table>
