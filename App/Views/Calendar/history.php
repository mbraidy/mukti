<?php
/* @var $data App\Models\Cat */
?>
<input type="hidden" id="imageLocation" value="/Assets/images/cat" />
<h3>Actions history and log</h3>
<table class="table table-sm table-condensed table-bordered table-responsive table-hover table-striped"
       id="main-table" data-update='/cat/updatecat' data-view='/cat/viewcat' data-image="/Assets/images/cat/">
    <thead class="thead-dark">
        <tr>
            <th>Adopter</th>
            <th>Date</th>
            <th>Time</th>
            <th>Name</th>
            <th>Race</th>
            <th>Gender</th>
            <th>Details</th>
            <th>Outcome</th>
        </tr>
    </thead>
    <?php foreach ( $schedule as $booking ) { ?>
        <tbody class="data-table" id='row-<?=$booking['id']?>'
               data-catid="<?=$booking['catID']?>"
               data-adopter="<?=$booking['userID']?>"
               data-date="<?=$booking['date']?>"
               data-timing="<?=$booking['timing']?>">
        <tr class="parent">
            <td><?=$booking['adaptor']?></td>
            <td><?=$booking['date']?></td>
            <td><?=$booking['timing']?></td>
            <td><?=$booking['name']?></td>
            <td><?=$booking['race']?></td>
            <td><?=$booking['gender']?></td>
            <td>
               <button type="button"
                       id='view-<?=$booking['id']?>'
                       class="btn btn-link"
                       title="View cat's and adopter's details">
                     <i class="far fa-eye fa-2x text-primary"></i>
               </button>
           </td>
           <td>
               <?php
               if ($booking['noShow'])
                   echo "No show";
               elseif ($booking['adopted'])
                    echo "Adopted";
               elseif ($booking['noInterest'])
                    echo "No interest";
               else
                    echo "";
               ?>
           </td>
        </tr>
        <tr class="child">
            <td colspan=8>
              <section class="showuser-container">
                  <label class="user-head-name"><b>Potential adopter: </b></label>
                  <p class="user-name"><?=$booking['adaptor']?></p>
                  <label class="user-head-email"><b>Email: </b></label>
                  <p class="user-email"><?=$booking['email']?></p>
                  <label class="user-head-phone"><b>Phone: </b></label>
                  <p class="user-phone"><?=$booking['phone']?></p>
              </section>
              <section class="showcat-container">
                <label class="cat-head-left"><b>Cat</b></label>
                <label class="cat-head-right"><b>Race</b></label>
                <p class="cat-input-left"><?=$booking['name']?></p>
                <p class="cat-input-right"><?=$booking['race']?></p>

                <label class="cat-head-left"><b>DoB</b><span id="age-<?=$booking['id']?>"></span></span></label>
                <label class="cat-head-right"><b>Color</b></label>
                <p class="cat-input-left"><?=$booking['dob']?> (<?=(int)$booking['age']?> days)</p>
                <p class="cat-input-right"><?=$booking['color']?></p>

                <label class="cat-head-left"><b>Chip number</b></label>
                <label class="cat-head-right"><b>Gender</b></label>
                <p class="cat-input-left"><?=$booking['chip']?></p>
                <p class="cat-input-right"><?=$booking['gender']?></p>

                <label class="cat-head-left"><b>Notes</b></label>
                <textarea class="cat-input-wide" disabled><?=$booking['note']?></textarea>

                <br/>
                <label class="cat-head-left"><b>Pictures of <?=$booking['name']?></b></label>


                <img src='/Assets/images/cat/<?=$booking['picture']?>'
                     class="cat-input-left zoom" alt="No image" height="100"/>

              </section>
            </td>
        </tr>
        </tbody>
    <?php } ?>
</table>
