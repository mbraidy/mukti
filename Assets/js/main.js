window.onload = function () {
	let fields = document.getElementsByClassName( "updatable_field" );
	Array.from(fields).forEach(function(element) {
	      element.addEventListener("change", update_profile);
	});
    
    $('.pagi-container>img').toggleClass('DefaultBigStyle1');
	$('.pagi-container label:nth-child(3)>img').trigger('click'); // This is to set initial Photo in the Frame.
	$('.menu ul li').on('click',function(){
		$('.menu ul li').removeClass();
		$(this).addClass('menuSelected');
		index=$(this).data('pattern');
		console.log('Pattern : '+index+' is Selected..!!');
		$('.pagi-container>img').removeClass();
		$('.pagi-container>img').addClass('DefaultBigStyle'+index); // Applying the CSS classes based on User's choice of transition styles.
	});
	$('.menu ul li:nth-child(1)').trigger('click');
	
	$('table').on('click', 'tr.parent .fa-eye', function(){
		  $(this).closest('tbody').toggleClass('open');
		});
}
$('#genericModal').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
    $(this).find('img').prop('src', "");
});
let change_password = function() {
	let oldpass = document.getElementById( "oldpass" );
	let password = document.getElementById( "password" );
	let pswRepeat = document.getElementById( "pswRepeat" );

	fetch( "/user/change_password", {
		  method: 'POST',
		  body:  "oldpass=" + oldpass.value + '&password=' + password.value + '&pswRepeat=' + pswRepeat.value,
		  headers:{
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "UPDATED":
			  let modal = document.getElementById( "genericModal" );
			  oldpass.value = '';
			  password.value = '';
			  pswRepeat.value = '';
			  modal.classList.remove('show');
			  break;
		  case "WRONG":
			  oldpass.value = '';
			  break;
		  case "REPEAT":
			  password.value = '';
			  pswRepeat.value = '';
			  break;
		  case "WAIT":
			  oldpass.value = '';
			  password.value = '';
			  pswRepeat.value = '';
			  break;
		  default:
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let add_cat = function( ) {
	let form = document.forms['embedded-form'];
	let fields = form.getElementsByClassName('input-field');
	let data = "";
	for (let i=0; i<fields.length; ++i) {
		if (typeof fields[i].name !== "undefined" && typeof fields[i].value !== "undefined")
			switch (fields[i].type) {
			  case "file":
					data += "&" + fields[i].name + "=" + fields[i].files[0].name;
					break;
			  case "date":
					data += "&" + fields[i].name + "='" + fields[i].value + "'";  
					break;
			  case "checkbox":
					data += "&" + fields[i].name + "=" + ((fields[i].value == 'on') ? 1 : 0);  
					break;
			  default: 
				  data += "&" + fields[i].name + "=" + fields[i].value;
			}
	}
	fetch( form.action, {
		  method: 'POST',
		  body: data.substr(1),
		  headers: {
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		let grid, row;
		switch ( response.result ) {
		  case "CREATED":
			  grid = document.getElementById( "main-table" );
			  row = grid.insertRow(-1);
			  row.innerHTML = response.data;
			  for (let i=0; i<fields.length; ++i) {
				  switch (fields[i].type) {
				  	case 'file':
				  		document.getElementById(`show-${fields[i].id}`).src = "";
				  		document.getElementById(fields[i].id).value = "";
				  		break;
				  	default:
				  		fields[i].value = "";
				  }
			  }
			  notification( "success", response.message );
			  break;
		  default:
			  notification( "failure", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let add_race = function( ) {
	let form = document.forms['embedded-form'];
	let fields = form.getElementsByClassName('input-field');
	let data = "";
	for (let i=0; i<fields.length; ++i) {
		if (typeof fields[i].name !== "undefined" && typeof fields[i].value !== "undefined")
			switch (fields[i].type) {
			  case "file":
					data += "&" + fields[i].name + "=" + fields[i].files[0].name;
					break;
			  case "date":
					data += "&" + fields[i].name + "='" + fields[i].value + "'";  
					break;
			  case "checkbox":
					data += "&" + fields[i].name + "=" + ((fields[i].value == 'on') ? 1 : 0);  
					break;
			  default: 
				  data += "&" + fields[i].name + "=" + fields[i].value;
			}
	}
	fetch( form.action, {
		  method: 'POST',
		  body: data.substr(1),
		  headers: {
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		let grid, row;
		switch ( response.result ) {
		  case "CREATED":
			  grid = document.getElementById( "main-table" );
			  row = grid.insertRow(-1);
			  row.innerHTML = response.data;
			  for (let i=0; i<fields.length; ++i) {
				  switch (fields[i].type) {
				  	case 'file':
				  		document.getElementById(`show-${fields[i].id}`).src = "";
				  		document.getElementById(fields[i].id).value = "";
				  		break;
				  	case 'range':
				  		document.getElementById(fields[i].id).value = 3;
				  		document.querySelector('#playfulnessOutput').value = 3;
				  		break;
				  	default:
				  		fields[i].value = "";
				  }
			  }
			  notification( "success", response.message );
			  break;
		  default:
			  notification( "failure", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let delete_row = function(clicked) {
	let confirm = window.confirm("Do you really want to delete thi item?");
	if (!confirm) return false;
	let body = {};
	let id = clicked.split("-")[1];
	body['id'] = id;
	let action = document.getElementById(clicked).dataset.ref;
	fetch( action, {
		  method: 'DELETE',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify( body )
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "DELETED":
			  document.getElementById(`tbody-${id}`).innerHTML = ""; 
			  notification( "success", response.message );
			  break;
		  default:
			  notification( "failure", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let update_row = function( changed, linkedto="" ) {
	let body = {};
	let tableRef;
	let newValue;

	switch ( typeof changed ) {
	  case "string":
		let target = document.getElementById(changed);
		let parts = changed.split("-");

		body['id'] = parts[1];
		body['field'] = parts[0];
		switch (target.tagName) {
			case "TEXTAREA":
			case "INPUT":
				if ( target.type && target.type == 'file' ) {
					newValue = body['value'] = target.files[0].name;
				} else if (target.type && target.type === 'checkbox') {
					newValue = body['value'] = target.checked;
				} else {
					newValue = body['value'] = target.value;
				}
				break;
			case "IMG":
				newValue = body['value'] = target.files[0].name;
				break;
			case 'SELECT':
				newValue = target.options[target.selectedIndex].text;
				body['value'] = target.options[target.selectedIndex].value;
				break;
			default:
				newValue = body['value'] = target.innerText;
		}
		break;
	  case "object":
		let	part1 = document.getElementById("modal-body").dataset.id;
		let	part2;
		let	part3;
		if ( changed.type == 'file' ){
			part2 = changed.id;
			newValue = part3 = changed.files[0].name;
		} else {
			part2 = changed.target.id;
			newValue = part3 = changed.target.value;
		}

		body['id'] = part1;
		body['field'] = part2;
		body['value'] = part3;

		tableRef = `${part2}-${part1}`;
		break;
	  default:
	}

	fetch( document.getElementById( "main-table" ).dataset.update, {
		  method: 'PUT',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify( body )
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "UPDATED":
			  if ( document.getElementById("imageLocation") ) {
				  var imageLocation = document.getElementById("imageLocation").value;
			  
				  if ( typeof changed != "string" ) {
					 if ( changed.type == 'file' ){
						document.getElementById(tableRef).src = `${imageLocation}/${newValue}`;
						document.getElementById(tableRef).classList.add('zoom');
					 } else {
					    document.getElementById(tableRef).value = newValue;
						document.getElementById(tableRef).innerText = newValue;
					 }
				  }
			  }
			  if (linkedto != '') {
				 let linked = document.getElementById(`${linkedto}_${changed}`);
				 switch (linked.tagName) {
				 	case 'IMG':
				 		linked.src = `${imageLocation}/${newValue}`;
						linked.classList.add('zoom');
						break;
				 	case 'TD':
				 		linked.innerText = newValue;
				 		// update_age
				 		break;
				 	case 'I':
				 		if (newValue) {
				 			linked.classList.remove('text-danger');
				 			linked.classList.add('text-success');
				 		} else {
				 			linked.classList.remove('text-success');
				 			linked.classList.add('text-danger');
				 		}
				 }
			  }
			  notification( "success", response.message );
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let view_row = function( clicked ) {
	let target = document.getElementById(clicked);
	let parts = clicked.split("-");
	let param = `?id=${parts[1]}&detail=1`;

	fetch( document.getElementById( "main-table" ).dataset.view + param )
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "FOUND":
			  let keys = Object.keys(response.data);
			  let imageLocation = document.getElementById("imageLocation").value;
			  $("#genericModal").modal();
			  document.getElementById("modal-body").setAttribute('data-id', response.data['id']);
			  document.getElementById("current-id").value =response.data['id'];
			  for ( let key of Object.keys(response.data) ) {
				  if ( document.getElementById(key) && response.data[key] !="" ) {
					 let entry = document.getElementById(key);
					 switch ( entry.type ) {
					   case "file":
						  entry.placeholder = response.data[key];
						  document.getElementById(`show-${key}`).src = `${imageLocation}/${response.data[key]}`;
						  document.getElementById(`show-${key}`).classList.add("blowout");
						  break;
					   case "checkbox":
						   if ( response.data[key] == 1 ) 
							   entry.checked = true;
						   else
							   entry.checked = false;
						   break;
					   default:
						   switch ( entry.tagName ) {
							  case "DIV":
								   if ( response.data[key] == 1 ) {
									    entry.innerHTML = 
									    	"<i class='far fa-calendar-check fa-3x'></i>"
								   } else {
									    entry.innerHTML = 
									    	"<i class='far fa-calendar-times fa-3x'></i>"
								   }
								   break;
							  case "IMG":
								   entry.placeholder = response.data[key];
								   document.getElementById(key).src = `${imageLocation}/${response.data[key]}`;
								   document.getElementById(key).classList.add("blowout");
								   break;
							  case "SECTION":
								  entry.innerText = response.data[key];
							  default:
								   entry.value = response.data[key];
//							   	   entry.addEventListener("change", update_row);
						   }
					  }
				  }
			  }
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let create_new = function( ) {
  $("#genericModal").modal();
  document.getElementById("registerbtn").style.display = 'block';
  let keys = document.getElementById('genericModal').getElementsByClassName('input-field');
  delete(document.getElementById("modal-body").dataset.id);
  for ( let key of keys ) {
	  document.getElementById(key.id).removeEventListener("change", update_row);
  }

  return false;
};
let bookTime = function( id ) {
	let parts = id.split("-");

	fetch( `/booking/book?id=${parts[1]}&detail=1` )
	.then( response => response.json() )
	.then( response => {
		switch ( response.result ) {
		  case "FOUND":
			  $("#genericModal").modal();
			  document.getElementById("book-appointment").dataset.cat = response.cat['id'];
			  document.getElementById("schedule-time").classList.add("hide-calendar");
			  document.getElementById("schedule-time").style.display = "none";
			  document.getElementById("meeting_date").value = null;
			  document.getElementById("cat-name").innerText = response.cat['name'];
			  document.getElementById("cat-race").innerText = response.cat['race'];
			  document.getElementById("cat-dob").innerText = response.cat['dob'];
			  document.getElementById("cat-color").innerText = response.cat['color'];
			  document.getElementById("cat-gender").innerText = response.cat['gender'];
			  document.getElementById("cat-chip").innerText = response.cat['chip'];
			  document.getElementById("cat-picture").src = `/Assets/images/cat/${response.cat['picture']}`;
			  document.getElementById("cat-note").innerText = response.cat['note'];

			  document.getElementById("race-name").innerText = response.race['name'] + ' (playfulness: ' + response.race['playfulness'] + ' )';
			  document.getElementById("race-picture1").src = `/Assets/images/race/${response.race['picture1']}`;
			  document.getElementById("race-picture2").src = `/Assets/images/race/${response.race['picture2']}`;
			  document.getElementById("race-note").innerText = response.race['note'];
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
};
let bookSelectDay = function( day ) {
	if (day == "") {
		document.getElementById("schedule-time").classList.add("hide-calendar");
		document.getElementById("schedule-time").style.display = "none";
	} else {
//		let timestamp = new Date(day).getTime() / 1000;
		fetch( `/booking/getSchedule?date=${day}` )
		.then( response => response.json() )
		.then( response => {
			switch ( response.result ) {
			  case "FOUND":
				  document.getElementById("schedule-time").classList.remove("hide-calendar");
				  document.getElementById("schedule-time").style.display = "block";
				  let now = new Date();
				  let hour = now.getHours();
				  let day = ("0" + now.getDate()).slice(-2);
				  let month = ("0" + (now.getMonth() + 1)).slice(-2);
				  let today = now.getFullYear() + "-" + (month) + "-" + (day);
				  for (let h of ['08', '09', '10', '11', '12', '13', '14', '15']) {
					  document.getElementById(`book-${h}`).disabled = false;
					  document.querySelector(`label[for='book-${h}']`).classList.add('radio_label');
					  document.querySelector(`label[for='book-${h}']`).innerText = h + ':00';
					  document.querySelector(`label[for='book-${h}']`).removeEventListener("click", removeTiming);
				  }
				  if ( response.day.user ) {
					  for (let timing of response['day']['user']) {
						  let parts = timing.split(":");
						  document.getElementById(`book-${parts[0]}`).disabled = true;
						  document.querySelector(`label[for='book-${parts[0]}']`).classList.remove('radio_label');
						  document.querySelector(`label[for='book-${parts[0]}']`).innerText = 'Your time';
						  document.querySelector(`label[for='book-${parts[0]}']`).addEventListener("click", removeTiming);
						}
				  }
				  if ( response.day.booked ) {
					  for (let timing of response['day']['booked']) {
						  let parts = timing.split(":");
						  document.getElementById(`book-${parts[0]}`).disabled = true;
						  document.querySelector(`label[for='book-${parts[0]}']`).classList.remove('radio_label');
						  document.querySelector(`label[for='book-${parts[0]}']`).innerText = 'Not available';
						}
				  }
				  break;
				}
		});
	}
};
let deleteTiming = function(ref) {
	let parts = ref.split('-');
	var placeholder = document.getElementById(`row-${parts[1]}`)
	let cat = placeholder.dataset.catid;
	let date = placeholder.dataset.date;
	let timing = placeholder.dataset.timing;
	fetch( "/booking/delete", {
		  method: 'DELETE',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify({date:date, timing:timing, cat:cat})
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "DELETED":
			  notification( "success", response.message );
			  placeholder.remove();
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let removeTiming = function(event) {
	var ref = event.target.htmlFor;
	let cat = document.getElementById("book-appointment").dataset.cat;
	let parts = ref.split('-');
	let timing = parts[1] + ':00';
	fetch( "/booking/delete", {
		  method: 'DELETE',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify({date:document.getElementById("meeting_date").value, timing:timing, cat:cat})
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "DELETED":
			  notification( "success", response.message );
			  document.getElementById(ref).disabled = false;
			  document.querySelector(`label[for='${ref}']`).classList.add('radio_label');
			  event.target.innerText = timing;
			  event.target.removeEventListener("click", removeTiming);
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let adoptCat = function(ref) {
	let parts = ref.split('-');
	var placeholder = document.getElementById(`row-${parts[1]}`)
	let adopter = placeholder.dataset.adopter;
	let cat = placeholder.dataset.catid;
	let date = placeholder.dataset.date;
	let timing = placeholder.dataset.timing;
	fetch( "/booking/adopt", {
		  method: 'PUT',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify({date:date, timing:timing, cat:cat, adopter:adopter})
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "ADOPTED":
			  notification( "success", response.message );
			  placeholder.remove();
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let noShow = function(ref) {
	let parts = ref.split('-');
	var placeholder = document.getElementById(`row-${parts[1]}`)
	let adopter = placeholder.dataset.adopter;
	let cat = placeholder.dataset.catid;
	let date = placeholder.dataset.date;
	let timing = placeholder.dataset.timing;
	fetch( "/booking/noshow", {
		  method: 'PUT',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify({date:date, timing:timing, cat:cat, adopter:adopter})
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "CLEARED":
			  notification( "success", response.message );
			  placeholder.remove();
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let noInterest = function(ref) {
	let parts = ref.split('-');
	var placeholder = document.getElementById(`row-${parts[1]}`)
	let adopter = placeholder.dataset.adopter;
	let cat = placeholder.dataset.catid;
	let date = placeholder.dataset.date;
	let timing = placeholder.dataset.timing;
	fetch( "/booking/nointerest", {
		  method: 'PUT',
		  headers: { "Content-type": "application/json" },
		  body: JSON.stringify({date:date, timing:timing, cat:cat, adopter:adopter})
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "CLEARED":
			  notification( "success", response.message );
			  placeholder.remove();
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};

let bookAppointment = function () {
	let date = document.getElementById("meeting_date").value;
	let timing = document.querySelectorAll("input[name=timing]:checked")[0].id;
	let cat = document.getElementById("book-appointment").dataset.cat;
	let parts = timing.split("-");
	fetch( "/booking/save", {
		  method: 'POST',
		  body: "date=" + date + '&timing=' + parts[1]+":00" + '&cat=' + cat,
		  headers: {
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "CREATED":
			  notification( "success", response.message );
			  document.querySelector('input[name="timing"]:checked').checked = false;
			  $('#genericModal').modal('toggle');
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let update_profile = function( changed ) {
	fetch( "/user/profile", {
		  method: 'POST',
		  body: "field=" + changed.target.name + '&value=' + changed.target.value,
		  headers: {
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		switch ( response.result ) {
		  case "UPDATED":
			  notification( "success", response.message );
			  break;
		  default:
			  notification( "warning", response.message, 3000 );
		}
	})
	.catch( error => console.error('Error:', error));
	return false;
};
let notification = function( theme, message, duration=2000 ) {
	let bkgd = "green";
	let frgd = "white";
	switch (theme) {
		case "success":
			bkgd = "#00cc44";
			frgd = "yellow";
			break;
		case "failure":
			bkgd = "red";
			frgd = "yellow";
			break;
		case "repeat":
			bkgd = "#ffff00";
			frgd = "blue";
			break;
		default:
	}
	snackbar.createSnackbar( message, {
		timeout: duration,
		theme: {
			backgroundColor: bkgd,
			textColor: frgd
		}
	});
}
let playfulnessUpdate = function(id, play, linkedto="") {
	if ( linkedto != '' ) {
		let parts = id.split("-");

		update_row( id, linkedto );
		document.querySelector(`#playfulnessOutput-${parts[1]}`).value = play;
	} else {
		document.querySelector('#playfulnessOutput').value = play;
	}
}
function handleFileSelect(evt, linkedto="") {
  let file = evt.files[0].name;
  if ( file != "" ) {
	  let location = document.getElementById('imageLocation').value;
	  if (document.getElementById(`output-${evt.id}`))
		  document.getElementById(`output-${evt.id}`).innerText = file;
	  document.getElementById(`show-${evt.id}`).src = `${location}/${file}`;
	  update_row( evt.id, linkedto );
  }
}
let update_age = function( dob ) {
	let current = new Date();
	let Bday = new Date( dob );
	if (Bday > current) return false;
	let days = Math.ceil((current - Bday + 1) / 86400000);

	let dayUnit = " day" + (( days > 1 ) ? "s" : "");
	let output = days + dayUnit;
	if ( days > 6 ) {
		let weeks = Math.ceil((days) / 7);
		let weekUnit = " week" + (( weeks > 1 ) ? "s" : "");
		output += " / " + weeks + weekUnit;
		if ( days > 30 ) {
			let months = Math.floor((days) / 30);
			let monthUnit = " month" + (( months > 1 ) ? "s" : "");
			output += " / " + months + monthUnit;
			if ( days > 365 ) {
				let years = Math.floor((days) / 365);
				let yearUnit = " year" + (( years > 1 ) ? "s" : "");
				output += " / " + years + yearUnit;
			}
		}
	} 
	document.getElementById('age').innerText = " (" + output + " )";
}
let signup = function() {
	let username = document.getElementById("username").value;
	if ( username.length < 4 ) {
		notification( "failure", "Usernamre must be at least four letters long", 4000 );
		return false;
	}

	let password = document.getElementById("password").value;
	if ( password.length < 4 ) {
		notification( "failure", "Password is a must", 4000 );
		return false;
	}

	let password_rpt = document.getElementById("password-repeat").value;
	if ( password_rpt != password ) {
		notification( "failure", "Passwords do not match" );
		return false;
	}
	if ( !document.getElementById("terms_accept").dataset.accept ) {
		notification( "failure", "You should accept the terms & conditions to be able to register", 4000 );
		return false;
	}
		
	fetch( document.getElementById("signupForm").action, {
		  method: 'POST',
		  body:  "username=" + username + '&password=' + password + '&role=user',
		  headers:{
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		  }
	})
	.then( response => response.json())
	.then( response => {
		if ( response.result == "REGISTERED" ) {
			notification( "success", "Welcome!  Please login to access the site!", 3000 );
			return false;
		} else if ( response.result == "DUPLICATE" )  {
			notification( "repeat", "It seems that you are already registered. Please login to access the site!", 3000 );
			return false;
		} else {
			notification( "failure", response.message, 4000 );
			return false;

		}

	})
	.catch( error => console.error('Error:', error));
	return false;
}
let term_accept = function() {
	let indicator = document.getElementById("terms_accept")
	if ( document.getElementById("agree_on_terms").checked ) {
		indicator.style.display = "block";
		indicator.hidden = false;
		indicator.dataset.accept = 1;
	} else {
		indicator.style.display = "none";
		indicator.hidden = true;
		indicator.dataset.accept = 0;
	}
}

